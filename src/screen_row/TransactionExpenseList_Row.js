import React from 'react';
import { View, TouchableOpacity, StyleSheet, Image, Text } from 'react-native';
import Colors from '../common/Colors';

const TransactionExpenseList_Row = (props) => {

    return (
        <TouchableOpacity style={[styles.container,styles.myshadow]}>
            <Text>{props.data_row.expense_date} </Text>
            <View>
                <Text style={styles.txtStyle}>{props.data_row.expense_amount} </Text>
                <Text>{props.data_row.expense_category} </Text>
                <Text>{props.data_row.expense_time} </Text>

            </View>

            <Text>{'Edit'} </Text>
        </TouchableOpacity>
    )
}
const styles = StyleSheet.create({

    container: {
        elevation: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        flex: 1,
        marginHorizontal: 10,
        paddingHorizontal: 5,
        marginVertical: 6,
        backgroundColor: 'white',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#FFFFFF',

    },
    myshadow: {
        shadowColor: '#0008',
        shadowOffset: {
            height: 1,
            width: 0,
        },
        shadowOpacity: 0.5,
        elevation: 2,
    },
    // myshadow: {
    //     shadowColor: '#0008',
    //     shadowOffset: {
    //         height: 1,
    //         width: 0,
    //     },
    //     shadowOpacity: 0.5,
    //     elevation: 2,
    // },

    imgStyle: {
        height: 25,
        width: 25
    },
    txtStyle:{
        color: Colors.redColor,
        fontWeight:'bold'
    }
});
export default TransactionExpenseList_Row;