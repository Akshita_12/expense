import React from 'react';
import { View, TouchableOpacity, StyleSheet, Image, Text } from 'react-native';
import Colors from '../common/Colors';


const TransactionIncomeList_Row = (props) => {

    return (
        <TouchableOpacity style={[styles.container,styles.myshadow]}>
            <Text>{props.data_row.income_date} </Text>
          <View>
              <Text style={styles.txtStyle}>{props.data_row.income_amount} </Text>
              <Text>{props.data_row.income_category} </Text>
              <Text>{props.data_row.income_time} </Text>

          </View>

          <Text>{'Edit'} </Text>
        </TouchableOpacity>
    )
}
const styles = StyleSheet.create({

    container: {
        elevation: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        flex: 1,
        marginHorizontal: 10,
        paddingHorizontal: 5,
        marginVertical: 6,
        backgroundColor: 'white',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#FFFFFF',

    },

    imgStyle: {
        height: 25,
        width: 25
    },
    txtStyle:{
        color:Colors.greenColor,
        fontWeight:'bold'
    }
});
export default TransactionIncomeList_Row;