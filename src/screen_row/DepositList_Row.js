import React from 'react';
import { View, TouchableOpacity, StyleSheet, Image, Text } from 'react-native';

const DepositList_Row = (props) => {

    return (
        <TouchableOpacity style={[styles.container,styles.myshadow]}>
            <Text>{props.data_row.deposit_date} </Text>
            <View>
                <Text>{props.data_row.deposit_from} </Text>
                <Text>{props.data_row.deposit_to} </Text>
                <Text>{props.data_row.deposit_amount} </Text>
                <Text>{props.data_row.deposit_time} </Text>
                <Text>{props.data_row.deposit_desc} </Text>

            </View>

            <Text>{'Edit'} </Text>
        </TouchableOpacity>
    )
}
const styles = StyleSheet.create({

    // container: {
    //     padding:15,
    //     backgroundColor: 'green',
    //     backgroundColor: '#fafaff',
    //     marginHorizontal:10,
    //     flexDirection:'row',
    //     justifyContent:'space-between',
    //     flex:1,
    //     elevation:4

    // },
    container: {
        elevation: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        flex: 1,
        marginHorizontal: 10,
        paddingHorizontal: 5,
        marginVertical: 6,
        backgroundColor: 'white',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#FFFFFF',

    },
    myshadow: {
        shadowColor: '#0008',
        shadowOffset: {
          height: 1,
          width: 0,
        },
        shadowOpacity: 0.5,
        elevation: 2,
      },
    imgStyle: {
        height: 25,
        width: 25
    }
});
export default DepositList_Row;