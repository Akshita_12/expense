import { executeSql } from './DatabaseExecutor';
import { run_migration_v1 } from './migration_v1';

export const run_database_migrations = async () => {
  console.log('running database migration');
  await run_migration_v1();
};



// Select record
export const selectAllRecord = async (query) => {
  let selectQuery = await executeSql(query, []);
  var rows = selectQuery.rows;
  var array = [];
  for (let i = 0; i < rows.length; i++) {
    array[i] = rows.item(i);
    console.log(array[i]);
  }
  return array;
};



// Update Record
export const updateService = async (id, name) => {
  const table_name = 'users_name';
  let query = `update ${table_name} set name = '${name}' where id = ${id} `;

  try {
    await executeSql(query, []);
    console.log('user updated');
  } catch (error) {
    console.log(error);
  }
};

// Delete record
export const deleteIncome = async () => {
  const table_name = 'income';
  // let query = `delete from ${table_name} where id = ${id}`;
  let query = `delete from ${table_name}`;
  // let query = `DROP TABLE ${table_name}`;

  try {
    await executeSql(query, []);
    console.log(table_name + ' deleted!');
  } catch (error) {
    console.log(error);
  }
};

// Delete record
export const deleteExpense = async () => {
  const table_name = 'expense';
  // let query = `delete from ${table_name} where id = ${id}`;
  let query = `delete from ${table_name}`;
  // let query = `DROP TABLE ${table_name}`;

  try {
    await executeSql(query, []);
    console.log(table_name + ' deleted!');
  } catch (error) {
    console.log(error);
  }
};

// Delete record
export const deleteDeposit = async () => {
  const table_name = 'deposit';
  // let query = `delete from ${table_name} where id = ${id}`;
  let query = `delete from ${table_name}`;
  // let query = `DROP TABLE ${table_name}`;

  try {
    await executeSql(query, []);
    console.log(table_name + ' deleted!');
  } catch (error) {
    console.log(error);
  }
};
//  insert income

export const insert_income = async (amount, category, date, time, description, payment_type) => {
  const table_name = 'income';
  let query = `insert into ${table_name}`;
  query += `(income_amount,income_category,income_date,income_time,income_description,income_payment_type)`;
  query += `VALUES (?,?,?,?,?,?)`;

  try {
    await executeSql(query, [amount, category, date, time, description, payment_type]);
    console.log(`Record inserted successfully ${table_name} `);

    selectAllRecord(`SELECT * FROM ${table_name}`);
  } catch (error) {
    console.log('Record is not inserted!');
  }
};

// insert expense

export const insert_expense = async (amount, category, date, time, description, payment_type) => {
  const table_name = 'expense';
  let query = `insert into ${table_name}`;
  query += `(expense_amount,expense_category,expense_date,expense_time,expense_description,expense_payment_type)`;
  query += `VALUES (?,?,?,?,?,?)`;

  try {
    await executeSql(query, [amount, category, date, time, description, payment_type]);
    console.log(`Record inserted successfully ${table_name} `);

    selectAllRecord(`SELECT * FROM ${table_name}`);
  } catch (error) {
    console.log('Record is not inserted!');
  }
};

// insert deposit

export const insert_deposit = async (from,to,amount,  date, time, description) => {
  const table_name = 'deposit';
  let query = `insert into ${table_name}`;
  query += `(deposit_from,deposit_to,deposit_amount,deposit_date,deposit_time,deposit_description)`;
  query += `VALUES (?,?,?,?,?,?)`;

  try {
    await executeSql(query, [from,to,amount,  date, time, description]);
    console.log(`Record inserted successfully ${table_name} `);

    selectAllRecord(`SELECT * FROM ${table_name}`);
  } catch (error) {
    console.log('Record is not inserted!');
  }
};


export const selectIncome = async () => {
  let result = await executeSql(`select SUM(income_amount) from income`, []);
  var rows = result.rows;
  var array = [];
  for (let i = 0; i < rows.length; i++) {
    array[i] = rows.item(i);
  }
  return rows.item(0);
};

export const selectExpense = async () => {
  
  let result = await executeSql(`select SUM(expense_amount) from expense`, []);
  var rows = result.rows;
  var array = [];
  for (let i = 0; i < rows.length; i++) {
    array[i] = rows.item(i);
  }
  return rows.item(0);
};

// sign up

// export const insert_signup = async (name,email,phone,pass,conf_pass) => {
//   const table_name = 'sign_up';
//   let query = `insert into ${table_name}`;
//   query += `(name,email,phone_no,pass,conf_pass)`;
//   query += `VALUES (?,?,?,?,?)`;
// // console.log(name,email,phone,pass,conf_pass);
//   try {
//     await executeSql(query, [name,email,phone,pass,conf_pass]);
//     console.log(`Record inserted successfully ${table_name} `);

//     selectAllRecord(`SELECT * FROM ${table_name}`);
//   } catch (error) {
//     console.log(error,'Record is not inserted!');
//   }
// };


export const insert_signup = async (name,email,phone,pass,conf_pass) => {
  const table_name = 'sign_up';
  let query = `insert into ${table_name}`;
  query += `(name,email,phone,pass,conf_pass)`;
  query += `VALUES (?,?,?,?,?)`;
  try {
    await executeSql(query, [name,email,phone,pass,conf_pass]);
    console.log(`Record inserted successfully ${table_name} `);

    // selectAllRecord(`SELECT * FROM ${table_name}`);
  } catch (error) {
    console.log(error,'Record is not inserted!');
  }
};