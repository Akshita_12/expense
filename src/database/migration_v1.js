import { executeSql } from './DatabaseExecutor';

const migration_name = `create_base_tables`;

export const run_migration_v1 = async () => {
  await create_income();
  await create_expense();
  await create_deposit();
  await create_signUp();
  // await create_login();
  // await create_services();
};

const create_services = async (db) => {
  let table_name = 'service_types';
  let query = `create table if not exists ${table_name} (`;
  query += `id INTEGER PRIMARY KEY AUTOINCREMENT,`;
  query += `service_name text )`;
  try {
    let results = await executeSql(query, []);
    console.log(`${table_name} created success`);
  } catch (error) {
    console.log(`creation error on ${table_name}` + JSON.stringify(error));
  }
};


const create_income = async (db) => {
  let table_name = 'income';
  let query = `create table if not exists ${table_name} (`;
  query += `income_id integer primary key AUTOINCREMENT,`;
  query += `income_amount integer,`;
  query += `income_category text, `;
  query += `income_date text, `;
  query += `income_time text, `;
  query += `income_description text, `;
  query += `income_payment_type text )`;

  try {
    let results = await executeSql(query, []);
    console.log(`created ${table_name} success`);
  } catch (error) {
    console.log(`creation error on ${table_name}` + JSON.stringify(error));
  }
};

const create_expense = async (db) => {
  let table_name = 'expense';
  let query = `create table if not exists ${table_name} (`;
  query += `expense_id integer primary key AUTOINCREMENT,`;
  query += `expense_amount integer,`;
  query += `expense_category text, `;
  query += `expense_date text, `;
  query += `expense_time text, `;
  query += `expense_description text, `;
  query += `expense_payment_type text )`;

  try {
    let results = await executeSql(query, []);
    console.log(`created ${table_name} success`);
  } catch (error) {
    console.log(`creation error on ${table_name}` + JSON.stringify(error));
  }
};

const create_deposit = async (db) => {
  let table_name = 'deposit';
  let query = `create table if not exists ${table_name} (`;
  query += `deposit_id integer primary key AUTOINCREMENT,`;
  query += `deposit_from text,`;
  query += `deposit_to text, `;
  query += `deposit_amount integer, `;
  query += `deposit_date text, `;
  query += `deposit_time text, `;
  query += `deposit_description text )`;

  try {
    let results = await executeSql(query, []);
    console.log(`created ${table_name} success`);
  } catch (error) {
    console.log(`creation error on ${table_name}` + JSON.stringify(error));
  }
};

const create_signUp = async (db) => {
  let table_name = 'sign_up';
  let query = `create table if not exists ${table_name} (`;
  query += `id integer primary key AUTOINCREMENT,`;
  query += `name text,`;
  query += `email text,`;
  query += `phone_no integer, `;
  query += `pass text, `;
  query += `conf_pass text )`;
  try {
    let results = await executeSql(query, []);
    console.log(`created ${table_name} success`);
  } catch (error) {
    console.log(`creation error on ${table_name}` + JSON.stringify(error));
  }
};

const create_login = async (db) => {
  let table_name = 'login';
  let query = `create table if not exists ${table_name} (`;
  query += `id integer primary key AUTOINCREMENT,`;
  query += ` login_id text,`;
  query += `login_pass text )`;

  try {
    let results = await executeSql(query, []);
    console.log(`created ${table_name} success`);
  } catch (error) {
    console.log(`creation error on ${table_name}` + JSON.stringify(error));
  }
};
