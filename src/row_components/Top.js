import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Colors from '../common/Colors'
import { getWidth } from '../common/Layout'

export const Top = (props) => {
    return (
        <View style={styles.container}>
            <Text style={styles.txtStyle}>{props.title} </Text>

            <View style={styles.mainStyle} >
                <View style={styles.smallView} />
                <View style={styles.halfView} />
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 100,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    txtStyle: {
        fontWeight: 'bold',
        marginLeft: 20,
        fontSize: 22,
        fontFamily: 'Roboto'
    },
    mainStyle: {
        flexDirection: "row",
        alignItems: "center",
        flex: 1,
        justifyContent: "flex-end"
    },
    smallView: {
        height: getWidth(30),
        width: getWidth(30),
        borderRadius: 100,
        marginRight: 10,
        backgroundColor: Colors.backgroundColor,
        elevation:10,
    },
    halfView: {
        height: getWidth(80),
        width: getWidth(80),
        marginRight: -40,
        borderRadius: 100,
        backgroundColor: Colors.backgroundColor,
        elevation:10,
    },

})