import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import Colors from '../common/Colors'
import { getWidth } from '../common/Layout'

export const Bottom = (props) => {
    return (
        <View style={styles.container}>
            <View style={styles.half} />
            <View style={styles.small} />

            <TouchableOpacity onPress={props.onPress}>
                <Text style={styles.txtStyle}>{props.title} </Text>
            </TouchableOpacity>

        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        height: 80,
        marginBottom: 10,
        marginHorizontal: 10,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    half: {
        height: getWidth(75),
        width: getWidth(75),
        marginLeft: -48,
        borderRadius: 100,
        backgroundColor: Colors.backgroundColor,
        elevation:10
    },
    small: {
        height: getWidth(30),
        width: getWidth(30),
        borderRadius: 100,
        backgroundColor: Colors.backgroundColor,
        marginRight: 190,
        elevation:10
    },
    txtStyle: {
        fontWeight: 'bold',
        fontSize: 22,
        fontFamily: 'Roboto'
    },
})