import React from 'react';
import { View, TouchableOpacity, StyleSheet, Image, Text, Dimensions } from 'react-native';
import Font from '../common/Font';
import { getWidth } from '../common/Layout';

const MySetting = (props) => {

    return (
        <View style={styles.container}>

            <TouchableOpacity
            onPress={props.onPress}
                style={styles.touchstyle}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <Text style={styles.txtStyle1}>
                        {props.title}
                    </Text>
                    <TouchableOpacity>
                        <Image
                            style={{ height: 25, width: 25, marginTop: 18 }}
                            source={require('../assets/right_icon.png')}
                        />
                    </TouchableOpacity>
                </View>

                <Text style={styles.txtStyle2}>
                    {props.description}
                </Text>
            </TouchableOpacity>

        </View>
    )
}
const { width, height } = Dimensions.get('window')

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        backgroundColor: 'white',

    },
    imgStyle: {
        height: 25,
        width: 25
    },
    touchstyle: {
        // height:40,
        // backgroundColor:'white',
        borderColor: 'grey',
        borderTopWidth: 0.5,
        paddingHorizontal: 10,
        marginHorizontal: 10
    },
    txtStyle1: {
        fontSize: 14,
        color: 'black',
        fontFamily: Font.LAOUI,
        fontWeight: 'bold'
    },
    txtStyle2: {
        fontSize: 12,
        color: 'grey',
        fontFamily: Font.Roboto,
        marginBottom: 10
    },

});

export default MySetting;

