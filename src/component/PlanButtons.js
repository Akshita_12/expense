import React, { Children, Component } from 'react';
import {
    View, Alert,
    StyleSheet,
    Image,
    ImageBackground,
    ScrollView,
    TouchableOpacity,
    Text,
    StatusBar
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Colors from '../common/Colors';
export default class PlanButtons extends Component {
    render() {
        const { ButtonColor, Textdata, onPress, style, Textcolor } = this.props;
        return (
            <TouchableOpacity 
            style={[styles.ButtonStyle, { ...style}]} 
            onPress={onPress}
            disabled={this.props.disabled}
            >
                <LinearGradient
                    colors={[Colors.red, Colors.orange]}
                    style={[{flex:1,height:'100%',alignItems:'center',justifyContent:'center'},styles.ButtonStyle]}
                    >
                <Text style={{ color: Textcolor ? Textcolor : "black", fontSize: 14, fontWeight: "500" }}>{Textdata}</Text>
               {this.props.children}
                </LinearGradient>
            </TouchableOpacity>
        )
    }
}
const styles = StyleSheet.create({
    ButtonStyle: {
        height: 30,
        width: "15%",
        borderRadius: 5,
        // marginTop: "15%",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
    }
})