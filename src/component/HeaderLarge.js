import React from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';
import {Image} from 'react-native';
import {Text} from 'react-native';
import Font from '../common/Font';
import Colors from '../common/Colors';


const HeaderLarge = (props) => {
  return (
    <View >
      <Text style={styles.txtStyle}>{props.title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  
  txtStyle: {
    fontSize: 32,
    color: '#FFFFFF',
    fontWeight:"600",
    fontFamily:Font.SourceSansPro,
    fontStyle:"normal",
    marginVertical: 34,
    marginHorizontal:31
  },
});
export default HeaderLarge;
