import { TouchableOpacity, Text, View, Image, StyleSheet } from 'react-native';
import React from 'react';
import { getWidth } from '../common/Layout';
import Colors from '../common/Colors';
import Font from '../common/Font';

const ProfileTopComponent = (props) => {
  return (
    <View style={{
      borderBottomWidth: 1,
      borderBottomColor: '#F5F5F5',

    }}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginTop: 27,
          marginHorizontal: 25
        }}>
        <TouchableOpacity
          onPress={props.onLogout}
          style={[{
            marginLeft: 10,
          }, styles.myshadow]}>
          <Image
            style={{
              width: getWidth(30),
              height: getWidth(30),
              // tintColor: Colors.backgroundColor
            }}
            source={props.logout_icon}
          />

        </TouchableOpacity>

        <TouchableOpacity
          onPress={props.onClickEdit}
          style={styles.myshadow}>
          <Image
            style={{
              width: getWidth(55),
              height: getWidth(55),

            }}
            source={props.edit_icon}
          />
        </TouchableOpacity>
      </View>

      <View
        style={{
          alignSelf: 'center',
          alignItems: 'center',
          marginTop: -35,
          marginVertical: 5,
        }}>
        <Image
          style={{
            width: 70,
            height: 70,
            borderRadius: 50,
            // marginLeft: getWidth(50)
          }}
          source={props.profile}
        />
        <Text style={styles.txtStyle} >{props.name} </Text>
        <Text style={{
          fontSize: getWidth(12),
          color: Colors.textColor,
          fontWeight: "normal",
          marginTop: 1
        }} >{"ID:\t" + props.id} </Text>
      </View>
    </View>

  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E5E5E5',
  },
  txtStyle: {
    fontWeight: "600",
    color: Colors.textDarkColor,
    fontSize: getWidth(25),
    color: Colors.grayColor,
    fontFamily: Font.SourceSansPro,
  },
  myshadow: {
    shadowColor: "#1050e6",
    shadowOpacity: 0.15,

    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 8.30,
    elevation: 13,
  },
});
export default ProfileTopComponent;
