import React, { useState } from "react";
import { View, Switch, StyleSheet, Text, TouchableOpacity } from "react-native";
import Colors from "../common/Colors";

export const SwitchComponent = (props) => {
    const [isEnabled, setIsEnabled] = useState(false);
    const toggleSwitch = () => setIsEnabled(previousState => !previousState);
  
    return (
        <View style={{ flexDirection: 'row', paddingHorizontal: 10, marginHorizontal: 10, justifyContent: 'space-between', alignItems: 'center', borderColor: 'grey', borderBottomWidth: 0.5 }}>

            <Text>
                {'Enable Pin / Fingerprint'}
            </Text>

            <TouchableOpacity>
                <Switch
                    trackColor={{ false: "#767577", true: "#81b0ff" }}
                    thumbColor={isEnabled ? Colors.textColor : "#f4f3f4"}
                    ios_backgroundColor="#3e3e3e"
                    onValueChange={toggleSwitch}
                    value={isEnabled}
                    style={{ height: 45, width: 45, marginTop: 18 }}
                />
            </TouchableOpacity>
        </View>
    )
}