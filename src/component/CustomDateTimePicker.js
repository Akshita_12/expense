import {
  TouchableOpacity,
  Text,
  StyleSheet,
  View,
  TextInput,
  PointPropType,
  Image,
} from 'react-native';
import React from 'react';
import Colors from '../common/Colors';
import Font from '../common/Font';
import DateTimePicker from 'react-native-modal-datetime-picker';

const CustomDateTimePicker = (props) => {
  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={[
        {
          borderBottomWidth:0.5,
          borderBottomColor:Colors.placeHolderColor,
          borderRadius: 2,
          // backgroundColor: 'white',
          height: 40,
          flexDirection: 'row',
          fontSize: Font.inputFont,
          justifyContent: 'space-between',
          alignItems: 'center',
          marginHorizontal:8,
        },
        props.style,
      ]}>
       
      <Text style={{color:Colors.placeHolderColor}} > {props.value} </Text>
      <Image
        source={props.right_icon}
        style={{height: 15, width: 15 }}
      />

      <DateTimePicker
      style={{
        shadowColor: '#fff',
        shadowRadius: 0,
        shadowOpacity: 1,
        shadowOffset: { height: 0, width: 0 },
      }}
          isVisible={props.isVisible}
          onConfirm={props.onConfirm}
          onCancel={props.onCancel}
          mode={props.mode}
          is24Hour={props.is24Hour}
          value={props.value}
          
          headerTextIOS={props.title}
          titleIOS={props.title}
          cancelTextIOS="Exit"
          confirmTextIOS="Ok"
        />

        
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  
});

export default CustomDateTimePicker;
