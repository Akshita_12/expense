import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Font from '../common/Font';
import { getWidth } from '../common/Layout';

const MyDropDownOption = (props) => {
    return (

        <View>
            <Text style={styles.txtStyle}>
                {props.label}
            </Text>
            <TouchableOpacity 
            onPress={props.onPress}
            style={{
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
                margin: 10,
                height: 40,
                padding: 10,
                backgroundColor: "white",
                elevation: 3,
                borderRadius: 5,
                marginVertical:props.marginVertical

            }} >
                <Text>
                    {props.category_type}
                </Text>
                <Image
                    source={require('../assets/drop_g.png')}
                    style={{ width: 20, height: 20 }}
                />
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'

    },
    txtStyle: {
        fontSize: 14,
        color: 'black',
        fontFamily: Font.Roboto,
        fontWeight: 'bold',
        marginHorizontal: 10,
        marginVertical:5
    },
    imgStyle: {
        height: getWidth(90),
        width: getWidth(90),

    }
});

export default MyDropDownOption;