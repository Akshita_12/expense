import { TouchableOpacity, Text, View, Image, StyleSheet, Dimensions } from 'react-native';
import React from 'react';
import Colors from '../common/Colors';
import Font from '../common/Font';
import { getWidth } from '../common/Layout';

const CustomAlert = (props) => {
  return (
    <View style={{
      flex: 1,
      width, height,
      position: 'absolute',
      backgroundColor: '#0005',
      alignItems: "center",
      justifyContent: "center"
    }}>
      <View style={{
        backgroundColor: 'white',
        height: getWidth(135),
        width: getWidth(238),
        borderRadius: 10,
        // alignItems:'center',
        justifyContent: "space-around"
      }}>
        {props.msg !== "" ?
          <Text style={styles.butontextStyle}>{props.msg} </Text>
          :
          <Text style={styles.butontextStyle} >
            {'Are you sure do you really want to go out ?'}
          </Text>
        }

        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-around',

          }}>
          <TouchableOpacity
            onPress={props.onPressFirst}

            style={[styles.alertButtonSyle, styles.myshadow, { backgroundColor: "#e5e5e5" }]}>
            <Text style={[styles.alertbtntextStyle, { color: '#1050E6', fontWeight: "600" }]}>

              {props.label_first}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={props.onPressSecond}

            style={[styles.alertButtonSyle, styles.myshadow,
            { backgroundColor: "#1050E6" }]} >

            <Text style={[styles.alertbtntextStyle, { color: "white" }]} >
              {props.label_second}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>

  );
};
const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  addbtnStyle: {
    width: 52,
    height: 52,
    marginBottom: 46,
    marginRight: 25,
    // flex: 1,
    // position: 'absolute',
    alignSelf: 'flex-end',
  },
  myshadow: {
    shadowColor: '#0008',
    shadowOffset: {
      height: 1,
      width: 0,
    },
    shadowOpacity: 0.5,
    elevation: 5,
  },
  butontextStyle: {
    color: Colors.textColor,
    fontSize: 14,
    textAlign: 'center',
    fontWeight: '500',
    alignSelf: "center",
    fontFamily: Font.SourceSansPro,
    width: getWidth(173),
  },
  alertButtonSyle: {
    width: 69,
    padding: 5,
    borderRadius: 22,
    backgroundColor: "white",
    alignItems: "center"
  },
  alertbtntextStyle: {
    color: Colors.textColor,
    fontSize: 14,
    fontWeight: 'normal',
    alignSelf: "center",
    fontFamily: Font.SourceSansPro,
  }
});
export default CustomAlert;
