import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Font from '../common/Font'

const Status = (props) => {
    return (
        <View style={styles.container} >
            <View style={{ alignItems: "center", justifyContent: "center" }} >
                <Text style={styles.txtStyle1} > {"💸 Income"}</Text>
                <Text style={styles.txtStyle1}>{props.incomes} </Text>
            </View>
            <View style={{ alignItems: "center", justifyContent: "center" }} >
                <Text style={styles.txtStyle2}> {"🖖 Expenses"}</Text>
                <Text style={styles.txtStyle2}>{props.expenses} </Text>
            </View>
            <View style={{ alignItems: "center", justifyContent: "center" }} >
                <Text style={styles.txtStyle3}> {"💰 Balance"}</Text>
                <Text style={styles.txtStyle3}>{props.balance} </Text>
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        height: 40,
        backgroundColor: "white",
        borderWidth: 0.5,
        borderColor: "lightgray",
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
        elevation:5
    },
    txtStyle1:{
        fontSize:11,
        color:'green',
        fontFamily:Font.LAOUI
    },
    txtStyle2:{
        fontSize:11,
        color:'red',
        fontFamily:Font.Roboto

    },
    txtStyle3:{
        fontSize:11,
        color:'orange',
        fontFamily:Font.Roboto

    }
})

export default Status;