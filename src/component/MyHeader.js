import React from 'react';
import { View, TouchableOpacity, StyleSheet, Image, Text } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { getWidth } from '../common/Layout';
handleViewRef = ref => view = ref;
bounce = () => view.bounceOutRight(2000);

const MyHeader = (props) => {

    return (
        <View style={{
            flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',
            height: 40, backgroundColor: 'white', elevation: 2
        }}>
            <TouchableOpacity>
                <Image
                    style={styles.imgStyle}
                    source={require('../assets/share.png')}
                />
            </TouchableOpacity>

            <TouchableOpacity>
                <Text style={{ fontSize: 16, fontWeight: 'bold' }}>
                    {'Daily Expenses'}
                </Text>
            </TouchableOpacity>

            <TouchableOpacity style={{ marginTop: 20 }}>
                <Animatable.Text
                    style={{ height: 40, width: 40,  }}
                    animation="zoomIn"
                    direction="normal"
                    iterationCount="infinite"
                    duration={2000}
                >{"💸 "}
                </Animatable.Text>
            </TouchableOpacity>



        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        height: getWidth(45),
        backgroundColor: '#5E35B1',
        flexDirection: 'row',
        alignItems: 'center',
    },
    imgStyle: {
        height: 25,
        width: 25
    }
});
export default MyHeader;