import { TouchableOpacity, Text, View, StyleSheet } from 'react-native';
import React from 'react';
import Colors from '../common/Colors';
import Font from '../common/Font';

const ProfileRow = (props) => {
  return (
    <View
      style={[{
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 40,
        marginTop: 25,
        borderBottomWidth: 0.3,
        borderBottomColor: Colors.placeHolderColor
      }, props.containerStyle]}>
      <Text style={[styles.labelText, props.textLabelStyle]} >{props.label}</Text>
      <Text style={[styles.valueText, props.textValueStyle]} >{props.value} </Text>
    </View>
  );
};

const styles = StyleSheet.create({

  labelText: {
    marginBottom: 7,
    color: Colors.textDarkColor,
    fontSize: 14,
    color: Colors.grayColor,
    fontFamily: Font.SourceSansPro,
  },
  valueText: {
    marginBottom: 7,
    color: Colors.textColor,
    fontSize: 14,
    fontFamily: Font.SourceSansProLight,
    fontWeight:"300",
    color: Colors.grayColor,
  }
});

export default ProfileRow;
