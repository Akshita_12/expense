import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import { Image } from 'react-native';
import { Text } from 'react-native';
import Colors from '../common/Colors';

const Header = (props) => {
  return (
    <View style={[styles.container, { justifyContent: 'space-between' }, props.containerStyle]}>
        <TouchableOpacity onPress={props.onPress}>
          {
            !props.isDisableBack ?
              <Image
                style={[{marginHorizontal:10},{
                  width: props.iconWidth,
                  resizeMode: "contain",
                  height: props.iconHeight,
                }]}
                source={require('../assets/back.png')}
              /> : null
          }
        </TouchableOpacity>
        <Text style={{ color: 'white', fontSize: 18,fontWeight:'bold', marginLeft: 10 }}>
          {props.status}
        </Text>


      <View style={{flexDirection:'row'}}>
        <TouchableOpacity onPress={props.onClick}>
          <Image
            source={props.image1}
            style={{ width: 22, marginRight: 10, height: 22 }}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={props.onClick}>
          <Image
            source={props.image2}
            style={{ width: 5, marginRight: 10, height: 25 }}
          />
        </TouchableOpacity>
      </View>
     
    </View>
  
  );
};

const styles = StyleSheet.create({
  container: {
    height: 35,
    backgroundColor: Colors.textColor,
    flexDirection: 'row',
    elevation:10,
    alignItems: 'center',
  },
});
export default Header;
