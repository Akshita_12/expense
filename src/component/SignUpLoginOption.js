import { TouchableOpacity, Text, View, Image, StyleSheet } from 'react-native';
import React from 'react';
import Colors from '../common/Colors';

const SignUpLoginOption = (props) => {
  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={[styles.bottomView, props.style]}>
      <Text style={styles.bottomTxtStyle}>
        {props.label}
      </Text>
      <View >
        <Text
          style={[
            styles.bottomTxtStyle,
            { fontWeight: 'bold', color: Colors.textColor },
          ]}>
          {props.clickableText}
        </Text>
      </View>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  bottomTxtStyle: {
    color: Colors.textColor,
    fontSize: 12
  },
  bottomView: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'center',
    backgroundColor: 'white',
    paddingVertical: 15,

  },

});
export default SignUpLoginOption;
