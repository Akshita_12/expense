import React from 'react';
import { View, TouchableOpacity, StyleSheet, Image, Text } from 'react-native';
import { getWidth } from '../common/Layout';


const Dashboard_Row = (props) => {
    return (
        <TouchableOpacity
            onPress={props.onPress}
            style={[styles.container,styles.myshadow]}>

            <Image
                style={styles.imgStyle}
                source={props.image}
            />
            <Text style={{ textAlign: 'center' }}>
                {props.text}
            </Text>
        </TouchableOpacity>
    )
}
const styles = StyleSheet.create({
    container: {
        flex:1,
        margin: 3,
        elevation: 8,
        borderRadius:3,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white'
    },

    imgStyle: {
        height: getWidth(45),
        width: getWidth(45),
    },

    myshadow:{
        shadowColor:"#1050e6",
        shadowOpacity:0.15,
        shadowOffset: {
          width: 0,
          height: 0,
        },
        shadowRadius: 8.30,
        elevation: 13,
      },
});
export default Dashboard_Row;