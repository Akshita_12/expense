import { TouchableOpacity, Text, View, Image, StyleSheet, Dimensions } from 'react-native';
import React from 'react';
import Colors from '../common/Colors';
import Font from "../common/Font";

import { getWidth } from '../common/Layout';
import MyInputText from '../common/MyInputText';

const ForgotPassword = (props) => {
  return (
    <View style={{
      flex: 1,
      width, height,
      position: 'absolute',
      backgroundColor: '#0005',
      alignItems: "center",
      justifyContent: "center"
    }}>
      <View style={{
        backgroundColor: 'white',
        height: getWidth(200),
        width: getWidth(285),
        borderRadius: 10,
        justifyContent: "space-around",
        // paddingVertical:60
      }}>

        <View>
          <MyInputText
            label="Email..."
            keyboardType="email-address"
            autoCapitalize="none"
            onChangeText={props.onChangeTextEmail}
            left_icon={require('../assets/email_g.png')}
            style={{ height: 40, }}
            value={props.value}
          />

          {/* <MyInputText
            label="Enter password"
            style={{ marginTop: 10, height: 40, marginTop: 30 }}
            onChangeText={props.onChangeTextPass}
            secureTextEntry={props.is_forgotPassword ? null : true}
            left_icon={require('../assets/lock_g.png')}
            right_icon={props.is_forgotPassword ? require('../assets/eye_off.png') : require('../assets/eye_on.png')}
          /> */}
        </View>

        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-around',
            marginBottom: 5
          }}>
          <TouchableOpacity
            onPress={props.onPressFirst}

            style={[styles.alertButtonSyle, styles.myshadow,{backgroundColor:"#E5E5E5"}]}>
            <Text style={[styles.alertbtntextStyle, { color: '#1050E6', fontWeight: "600" }]}>
              {props.label_first}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={props.onPressSecond}

            style={[styles.alertButtonSyle, styles.myshadow,
            { backgroundColor: "#1050E6" }]} >
            <Text style={[styles.alertbtntextStyle, { color: "white" }]} >
              {props.label_second}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>

  );
};
const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  addbtnStyle: {
    width: 52,
    height: 52,
    marginBottom: 46,
    marginRight: 25,
    // flex: 1,
    // position: 'absolute',
    alignSelf: 'flex-end',
  },
  myshadow:{
    shadowColor:"#1050e6",
    shadowOpacity:0.15,
    
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 8.30,
    elevation: 13,
  },
  butontextStyle: {
    color: Colors.textColor,
    fontSize: 14,
    textAlign: 'center',
    fontWeight: 'normal',
    alignSelf: "center",
    fontFamily: Font.SourceSansPro,
    height: 33,
    width: 173,
  },
  alertButtonSyle: {
    width: 100,
    height: 32,
    borderRadius: 22,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent:"center"
  },
  alertbtntextStyle: {
    color: Colors.textColor,
    fontSize: 14,
    fontWeight: 'normal',
    alignSelf: "center",
    fontFamily: Font.SourceSansPro,
  }
});
export default ForgotPassword;
