
import React from 'react';
import { Image, ScrollView, StyleSheet, Text, TouchableOpacity } from "react-native";

const ListScreen_Row = (props) => {
    return (
        <TouchableOpacity 
        onPress={props.onPress}
        style={styles.rowStyle}>
            <Image
                style={styles.imageStyle}
                source={props.image}
            />
            <Text style={{textAlign:'center',marginTop:5,fontFamily:'serif',fontSize:17}}>
                {props.text}
            </Text>

        </TouchableOpacity>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fafaff',
    },
    imageStyle: {
        height: 35,
        width: 35,
        marginRight: 20,
    },
    rowStyle:{
        flexDirection: 'row',
        backgroundColor: 'white',
        padding:20,
        marginHorizontal:10,
        marginTop: 11,
        elevation:5,
        borderRadius: 5,
    }
});


export default ListScreen_Row;