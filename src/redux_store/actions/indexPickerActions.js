import {
  SET_ADD_EXPENSE_DATE,
  SET_ADD_EXPENSE_DATE_VISIBLE,
  SET_ADD_EXPENSE_TIME,
  SET_ADD_INCOME_DATE,
  SET_ADD_INCOME_DATE_VISIBLE,
  SET_ADD_INCOME_TIME,
  SET_DEPOSIT_DATE,
  SET_DEPOSIT_DATE_VISIBLE,
  SET_DEPOSIT_TIME,
  SET_DEPOSIT_TIME_VISIBLE,
  SET_EXPENSE_TIME_VISIBLE,
  SET_INCOME_TIME_VISIBLE
} from "./types";

// income time picker

export const handleIncomePickerTime = (time) => (dispatch) => {
  dispatch({
    type: SET_ADD_INCOME_TIME,
    // payload: moment(datetime).format('h:mm a'),
    payload: time,
  });
  dispatch({
    type: SET_INCOME_TIME_VISIBLE,
    payload: false,
  });
};

export const showIncomePicker = () => (dispatch) => {
  dispatch({
    type: SET_INCOME_TIME_VISIBLE,
    payload: true,
  });
};

export const hideIncomePicker = () => (dispatch) => {
  dispatch({
    type: SET_INCOME_TIME_VISIBLE,
    payload: false,
  });
};

//  income date picker
export const setAddIncomeDate = (date) => (dispatch) => {

  dispatch({
    type: SET_ADD_INCOME_DATE,
    // payload: moment(datetime).format('h:mm a'),
    payload: date,
  });
  dispatch({
    type: SET_ADD_INCOME_DATE_VISIBLE,
    payload: false,
  });
}

export const showPickerIncomeDate = () => (dispatch) => {
  dispatch({
    type: SET_ADD_INCOME_DATE_VISIBLE,
    payload: true,
  });
};

export const hideIncomePickerDate = () => (dispatch) => {
  dispatch({
    type: SET_ADD_INCOME_DATE_VISIBLE,
    payload: false,
  });
};


// expense time picker

export const handleExpensePickerTime = (time) => (dispatch) => {
  dispatch({
    type: SET_ADD_EXPENSE_TIME,
    // payload: moment(datetime).format('h:mm a'),
    payload: time,
  });
  dispatch({
    type: SET_EXPENSE_TIME_VISIBLE,
    payload: false,
  });
};

export const showExpensePicker = () => (dispatch) => {
  dispatch({
    type: SET_EXPENSE_TIME_VISIBLE,
    payload: true,
  });
};

export const hideExpensePicker = () => (dispatch) => {
  dispatch({
    type: SET_EXPENSE_TIME_VISIBLE,
    payload: false,
  });
};

// expense date picker
export const setAddExpenseDate = (date) => (dispatch) => {

  dispatch({
    type: SET_ADD_EXPENSE_DATE,
    // payload: moment(datetime).format('h:mm a'),
    payload: date,
  });
  dispatch({
    type: SET_ADD_EXPENSE_DATE_VISIBLE,
    payload: false,
  });
}

export const showPickerExpenseDate = () => (dispatch) => {
  dispatch({
    type: SET_ADD_EXPENSE_DATE_VISIBLE,
    payload: true,
  });
};

export const hideExpensePickerDate = () => (dispatch) => {
  dispatch({
    type: SET_ADD_EXPENSE_DATE_VISIBLE,
    payload: false,
  });
};


// expense date picker
export const handleDepositDatePicker = (date) => (dispatch) => {

  dispatch({
    type: SET_DEPOSIT_DATE,
    // payload: moment(datetime).format('h:mm a'),
    payload: date,
  });
  dispatch({
    type: SET_DEPOSIT_DATE_VISIBLE,
    payload: false,
  });
}

export const showPickerDepositDate = () => (dispatch) => {
  dispatch({
    type: SET_DEPOSIT_DATE_VISIBLE,
    payload: true,
  });
};

export const hidePickerDepositDate = () => (dispatch) => {
  dispatch({
    type: SET_DEPOSIT_DATE_VISIBLE,
    payload: false,
  });
};
// deposit time picker

export const handleDepositTimePicker = (time) => (dispatch) => {
  dispatch({
    type: SET_DEPOSIT_TIME,
    // payload: moment(datetime).format('h:mm a'),
    payload: time,
  });
  dispatch({
    type: SET_DEPOSIT_TIME_VISIBLE,
    payload: false,
  });
};

export const showDepositTimePicker = () => (dispatch) => {
  dispatch({
    type: SET_DEPOSIT_TIME_VISIBLE,
    payload: true,
  });
};

export const hideDepositTimePicker = () => (dispatch) => {
  dispatch({
    type: SET_EXPENSE_TIME_VISIBLE,
    payload: false,
  });
};



