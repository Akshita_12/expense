import { APPOINTMENT_DATE_VISIBLE, APPOINTMENT_TIMING_VISIBLE, GET_NAME, IS_DELETE_MESSAGE, IS_DROPDOWN_ENABLED, IS_DROPDOWN_ENABLED_2, IS_FORGOT_PASSWORD, IS_VISIBLE, IS_VISIBLE_CONF_PASS, IS_VISIBLE_PASS, SET_ADD_EXPENSE_AMOUNT, SET_ADD_EXPENSE_CATEGORY, SET_ADD_EXPENSE_DATE, SET_ADD_EXPENSE_DATE_VISIBLE, SET_ADD_EXPENSE_DESC, SET_ADD_EXPENSE_PAYMENT_TYPE, SET_ADD_EXPENSE_TIME, SET_ADD_INCOME_AMOUNT, SET_ADD_INCOME_CATEGORY, SET_ADD_INCOME_DATE, SET_ADD_INCOME_DATE_VISIBLE, SET_ADD_INCOME_DESC, SET_ADD_INCOME_PAYMENT_TYPE, SET_ADD_INCOME_TIME, SET_APPOINTMENT_DATE, SET_APPOINTMENT_TIME, SET_BALANCE, SET_CATEGORY_TYPE, SET_CONF_PASS, SET_DASHBOARD_PROFILE, SET_DEPOSIT_AMOUNT, SET_DEPOSIT_DATE, SET_DEPOSIT_DATE_VISIBLE, SET_DEPOSIT_DESC, SET_DEPOSIT_FROM, SET_DEPOSIT_LIST, SET_DEPOSIT_TIME, SET_DEPOSIT_TO, SET_DESC, SET_DROPDOWN_VISIBILITY, SET_EMAIL, SET_EXPENSE, SET_EXPENSE_LIST, SET_INCOME, SET_INCOME_LIST, SET_KEYBOARD_AVOIDING, SET_LOGIN_ID, SET_LOGIN_PASS, SET_NAME, SET_PASSWORD, SET_PAYMENT_TYPE, SET_PHONE_NO, SET_PROFILE_CONTACT, SET_PROFILE_LOGO_URL, SET_PROFILE_NAME, SET_USER_LIST, } from './types';
import store from '../store';
import { deleteDeposit, deleteExpense, deleteIncome, insert_deposit, insert_expense, insert_income, selectAllRecord, selectExpense, selectIncome, selectSumOfColumn } from '../../database/AppDatabase';

export const setDesc = (desc) => (dispatch) => {
  dispatch({
    type: SET_DESC,
    payload: desc,
  })
}

// custome date time 

export const handlePickerDate = (date) => (dispatch) => {
  dispatch({
    type: SET_APPOINTMENT_DATE,
    // payload: moment(datetime).format('h:mm a'),
    payload: date,
  });
  dispatch({
    type: APPOINTMENT_DATE_VISIBLE,
    payload: false,
  });
};

export const handlePickerTime = (time) => (dispatch) => {
  dispatch({
    type: SET_APPOINTMENT_TIME,
    // payload: moment(datetime).format('h:mm a'),
    payload: time,
  });
  dispatch({
    type: APPOINTMENT_TIMING_VISIBLE,
    payload: false,
  });
};
// show picker time
export const showPicker = () => (dispatch) => {
  dispatch({
    type: APPOINTMENT_TIMING_VISIBLE,
    payload: true,
  });
};

export const hidePicker = () => (dispatch) => {
  dispatch({
    type: APPOINTMENT_TIMING_VISIBLE,
    payload: false,
  });
};

// export const showPickerDate = () => (dispatch) => {
//   dispatch({
//     type: APPOINTMENT_DATE_VISIBLE,
//     payload: true,
//   });
// };

export const hidePickerDate = () => (dispatch) => {
  dispatch({
    type: APPOINTMENT_DATE_VISIBLE,
    payload: false,
  });
};

//  set dropdown

export const setDropdown = (isVisibleDropdown) => (dispatch) => {
  dispatch({
    type: SET_DROPDOWN_VISIBILITY,
    payload: isVisibleDropdown,
  });
};
export const setDropdownVisibility = (isVisible) => (dispatch) => {
  dispatch({
    type: IS_DROPDOWN_ENABLED,
    payload: isVisible,
  });
};

export const setCategoryType = (category_type) => (dispatch) => {
  dispatch({
    type: SET_CATEGORY_TYPE,
    payload: category_type,
  });
};

export const setPaymentType = (payment_type) => (dispatch) => {
  dispatch({
    type: SET_PAYMENT_TYPE,
    payload: payment_type,
  });
};

export const setDropdownVisibility2 = (isVisible) => (dispatch) => {
  dispatch({
    type: IS_DROPDOWN_ENABLED_2,
    payload: isVisible,
  });
};

export const setIsVisible = (isVisible) => (dispatch) => {
  console.log('................................');
  dispatch({

    type: IS_VISIBLE,
    payload: isVisible,
  });
};
export const isDeleteMessage = (is_delete) => (dispatch) => {
  dispatch({
    type: IS_DELETE_MESSAGE,
    payload: is_delete,
  });
};

// SET ADD INCOME 

export const setAddIncomeAmount = (amount) => (dispatch) => {
  dispatch({
    type: SET_ADD_INCOME_AMOUNT,
    payload: amount,
  })
}

export const setAddIncomeCategory = (category) => (dispatch) => {
  dispatch({
    type: SET_ADD_INCOME_CATEGORY,
    payload: category,
  })
}


export const setAddIncomeTime = (time) => (dispatch) => {
  dispatch({
    type: SET_ADD_INCOME_TIME,
    payload: time,
  })
}

export const setAddIncomeDesc = (desc) => (dispatch) => {
  dispatch({
    type: SET_ADD_INCOME_DESC,
    payload: desc,
  })
}

export const setAddIncomePaymentType = (payment) => (dispatch) => {
  dispatch({
    type: SET_ADD_INCOME_PAYMENT_TYPE,
    payload: payment,
  })
}

// SET ADD EXPENSE

export const setAddExpenseAmount = (amount) => (dispatch) => {
  dispatch({
    type: SET_ADD_EXPENSE_AMOUNT,
    payload: amount,
  })
}

export const setAddExpenseCategory = (category) => (dispatch) => {
  dispatch({
    type: SET_ADD_EXPENSE_CATEGORY,
    payload: category,
  })
}


export const setAddExpenseTime = (time) => (dispatch) => {
  dispatch({
    type: SET_ADD_EXPENSE_TIME,
    payload: time,
  })
}

export const setAddExpenseDesc = (desc) => (dispatch) => {
  dispatch({
    type: SET_ADD_EXPENSE_DESC,
    payload: desc,
  })
}

export const setAddExpensePaymentType = (payment) => (dispatch) => {
  dispatch({
    type: SET_ADD_EXPENSE_PAYMENT_TYPE,
    payload: payment,
  })
}


//  add income insert database

export const setInsertAddIncome = (amount, category, date, time, description, payment_type) => (dispatch) => {
  insert_income(amount, category, date, time, description, payment_type).then(() => {
    console.log('insert income');
  })
  selectAllRecord(`SELECT * FROM income`).then((res) => {
    dispatch({
      type: SET_INCOME_LIST,
      payload: res
    })
  })
};


// add expense insert database

export const setInsertAddExpense = (amount, category, date, time, description, payment_type) => (dispatch) => {
  insert_expense(amount, category, date, time, description, payment_type).then(() => {
    console.log('insert expense');
  })
  selectAllRecord("SELECT * FROM  expense").then((res) => {
    dispatch({
      type: SET_EXPENSE_LIST,
      payload: res
    })
  })
};

// add deposit insert database

export const setInsertAddDeposit = (deposit_from, deposit_to, deposit_amount, deposit_date, deposit_time, deposit_desc) => (dispatch) => {
  insert_deposit(deposit_from, deposit_to, deposit_amount, deposit_date, deposit_time, deposit_desc).then(() => {
    console.log(res);
  })
  selectAllRecord("SELECT * FROM deposit").then((res) => {
    dispatch({
      type: SET_DEPOSIT_LIST,
      payload: res
    })
  })
};

// insert signup

export const setInsertSignUp = (name, email, phone, pass, conf_pass) => (dispatch) => {
  insert_signup(name, email, phone, pass, conf_pass).then(() => {
    // console.log('insert signup');
  })
  selectAllRecord(`SELECT * FROM sign_up`).then((res) => {
    dispatch({
      type: SET_USER_LIST,
      payload: res
    })
  })
};

// dashboard status action

export const setIncome = () => (dispatch) => {
  selectIncome().then((res) => {
    const value = Object.values(res);
    dispatch({
      type: SET_INCOME,
      payload: value[0],
    })
  })
}

export const setExpense = () => (dispatch) => {
  selectExpense().then((res) => {
    const value = Object.values(res);
    dispatch({
      type: SET_EXPENSE,
      payload: value[0],
    })
  })
}

export const setBalance = () => (dispatch) => {
  let state = store.getState().indexReducer;
  let income = state.income;
  let expense = state.expense;
  let balance = income - expense
  dispatch({
    type: SET_BALANCE,
    payload: balance,
  })
}

// DELETE ALL INCOME,EXPENSES,DEPOSIT

export const deleteAllIncome = () => (dispatch) => {
  deleteIncome().then((res) => {
    // console.log("row deleted");
  })
}
export const deleteAllExpense = () => (dispatch) => {
  deleteExpense().then((res) => {
    // console.log("row deleted");
  })
}
export const deleteAllDeposit = () => (dispatch) => {
  deleteDeposit().then((res) => {
    // console.log("row deleted");
  })
}

// set login
export const setLoginId = (login_id) => (dispatch) => {
  dispatch({
    type: SET_LOGIN_ID,
    payload: login_id,
  });
};

export const setLoginPass = (login_pass) => (dispatch) => {
  dispatch({
    type: SET_LOGIN_PASS,
    payload: login_pass,
  });
};

// FORGOT PASSWORD

export const setIsForgotPassword = (isTrue) => (dispatch) => {
  dispatch({
    type: IS_FORGOT_PASSWORD,
    payload: isTrue,
  });
};

export const setPasswordVisibility = (isVisibile) => (dispatch) => {
  dispatch({
    type: IS_VISIBLE_PASS,
    payload: isVisibile,
  });
};

export const setKeyboardAvoiding = (inEnabled) => (dispatch) => {
  dispatch({
    type: SET_KEYBOARD_AVOIDING,
    payload: inEnabled,
  });
};

export const setConfPassVisibility = (isVisibile) => (dispatch) => {
  dispatch({
    type: IS_VISIBLE_CONF_PASS,
    payload: isVisibile,
  })
}

// sign up
export const setName = (name) => (dispatch) => {
  dispatch({
    type: SET_NAME,
    payload: name,
  });
};

export const setEmail = (email) => (dispatch) => {
  dispatch({
    type: SET_EMAIL,
    payload: email,
  });
};

export const setPhone = (phone) => (dispatch) => {
  dispatch({
    type: SET_PHONE_NO,
    payload: phone,
  });
};

export const setPass = (pass) => (dispatch) => {
  dispatch({
    type: SET_PASSWORD,
    payload: pass,
  });
};

export const setConfirmPass = (conf_pass) => (dispatch) => {
  dispatch({
    type: SET_CONF_PASS,
    payload: conf_pass,
  });
};

// deposit action

export const setDepositFrom = (deposit_from_type) => (dispatch) => {
  dispatch({
    type: SET_DEPOSIT_FROM,
    payload: deposit_from_type,
  })
}

export const setDepositTo = (to) => (dispatch) => {
  dispatch({
    type: SET_DEPOSIT_TO,
    payload: to,
  })
}

export const setDepositAmount = (amount) => (dispatch) => {
  dispatch({
    type: SET_DEPOSIT_AMOUNT,
    payload: amount,
  })
}

export const setDepositTime = (time) => (dispatch) => {
  dispatch({
    type: SET_DEPOSIT_TIME,
    payload: time,
  })
}

export const setDepositDesc = (desc) => (dispatch) => {
  dispatch({
    type: SET_DEPOSIT_DESC,
    payload: desc,
  })
}

// profile ation

export const setProfileName = (name) => (dispatch) => {
  let state = store.getState().indexReducer;
  dispatch({
    type: SET_PROFILE_NAME,
    payload: name,
  });
};

export const setProfileContact = (contact) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_CONTACT,
    payload: contact,
  });
};

export const setProfileEmail = (email) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_EMAIL,
    payload: email,
  });
};


export const setProfileAddress = (address) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_ADDRESS,
    payload: address,
  });
};

export const setProfileLogo = (logo) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_LOGO_URL,
    payload: logo,
  });
};

export const setDashboardProfile = (profile) => (dispatch) => {
  dispatch({
    type: SET_DASHBOARD_PROFILE,
    payload: profile,
  });
};

// update profile
// export const setUpdateProfilePic = (pic) => (dispatch) => {
//   dispatch({
//     type: SET_UPDATE_PROFILE_PIC,
//     payload: pic,
//   });
//   dispatch({
//     type: IS_UPDATE_PROFILE_VISIBLE,
//     payload: true,
//   });
// };
