import { SET_ADD_EXPENSE_DATE, SET_ADD_EXPENSE_DATE_VISIBLE, SET_ADD_EXPENSE_TIME, SET_ADD_INCOME_DATE, SET_ADD_INCOME_DATE_VISIBLE, SET_ADD_INCOME_TIME, SET_DEPOSIT_DATE, SET_DEPOSIT_DATE_VISIBLE, SET_DEPOSIT_TIME, SET_DEPOSIT_TIME_VISIBLE, SET_EXPENSE_TIME_VISIBLE, SET_INCOME_TIME_VISIBLE } from "../actions/types";


const initialState = {

    // income date var
    income_date: 'dd/mm/yy',
    isVisibleAddIncomeDate: false,

    // income time var
    income_time: 'hh:mm:ss',
    isIncomeTimeVisibility: false,

    // expense date var
    expense_date: 'dd/mm/yy',
    isVisibleAddExpenseDate: false,

    // expense time var
    isExpenseTimeVisibility: false,
    expense_time: 'hh:mm:ss',

     // deposit date var
     deposit_date: 'dd/mm/yy',
     isVisibleDepositDate: false,
 
     // deposit time var
     deposit_time: 'hh:mm:ss',
     isDepositTimeVisibility: false,
};

export default function (state = initialState, action) {
    switch (action.type) {

        // income date cases
        case SET_ADD_INCOME_DATE:
            return {
                ...state,
                income_date: action.payload,
            };

        case SET_ADD_INCOME_DATE_VISIBLE:
            return {
                ...state,
                isVisibleAddIncomeDate: action.payload,
            };

        //   income time cases
        case SET_INCOME_TIME_VISIBLE:
            return {
                ...state,
                isIncomeTimeVisibility: action.payload,
            };

        case SET_ADD_INCOME_TIME:
            return {
                ...state,
                income_time: action.payload,
            };

        // expense date cases
        case SET_ADD_EXPENSE_DATE:
            return {
                ...state,
                expense_date: action.payload,
            };

        case SET_ADD_EXPENSE_DATE_VISIBLE:
            return {
                ...state,
                isVisibleAddExpenseDate: action.payload,
            };

        // expense time cases
        case SET_ADD_EXPENSE_TIME:
            return {
                ...state,
                expense_time: action.payload,
            };
        case SET_EXPENSE_TIME_VISIBLE:
            return {
                ...state,
                isExpenseTimeVisibility: action.payload,
            };


        // expense date cases
        case SET_DEPOSIT_DATE:
            return {
                ...state,
                deposit_date: action.payload,
            };

        case SET_DEPOSIT_DATE_VISIBLE:
            return {
                ...state,
                isVisibleDepositDate: action.payload,
            };

        // deposit time cases
        case SET_DEPOSIT_TIME:
            return {
                ...state,
                deposit_time: action.payload,
            };
        case SET_DEPOSIT_TIME_VISIBLE:
            return {
                ...state,
                isDepositTimeVisibility: action.payload,
            };
        default:
            return state;
    }
}