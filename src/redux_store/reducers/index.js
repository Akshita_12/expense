import {combineReducers} from 'redux';
import indexPickerReducer from './indexPickerReducer';
import indexReducer from './indexReducer';



export default combineReducers({
    indexReducer: indexReducer,
    indexPickerReducer:indexPickerReducer,
})
