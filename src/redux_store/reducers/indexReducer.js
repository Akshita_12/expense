import {
  APPOINTMENT_DATE_VISIBLE, APPOINTMENT_TIMING_VISIBLE,
  IS_DELETE_MESSAGE, IS_DROPDOWN_ENABLED, IS_DROPDOWN_ENABLED_2,
  IS_FORGOT_PASSWORD, IS_VISIBLE, IS_VISIBLE_CONF_PASS, IS_VISIBLE_PASS,
  SET_ADD_EXPENSE_AMOUNT, SET_ADD_EXPENSE_CATEGORY,
   SET_ADD_EXPENSE_DESC, SET_ADD_EXPENSE_PAYMENT_TYPE, SET_ADD_INCOME_AMOUNT, SET_ADD_INCOME_CATEGORY,
 SET_ADD_INCOME_DESC,
  SET_ADD_INCOME_PAYMENT_TYPE, SET_APPOINTMENT_DATE,
  SET_APPOINTMENT_TIME, SET_BALANCE, SET_CATEGORY_TYPE, SET_CONF_PASS,
  SET_DASHBOARD_PROFILE,
  SET_DEPOSIT_AMOUNT, SET_DEPOSIT_DATE, SET_DEPOSIT_DATE_VISIBLE,
  SET_DEPOSIT_DESC, SET_DEPOSIT_FROM, SET_DEPOSIT_LIST,
  SET_DEPOSIT_TIME, SET_DEPOSIT_TO, SET_DESC, SET_DROPDOWN_VISIBILITY,
  SET_EMAIL, SET_EXPENSE, SET_EXPENSE_LIST, SET_INCOME, SET_INCOME_LIST, SET_KEYBOARD_AVOIDING, SET_LOGIN_ID,
  SET_LOGIN_PASS, SET_NAME, SET_PASSWORD, SET_PAYMENT_TYPE, SET_PHONE_NO, SET_PROFILE_ADDRESS, SET_PROFILE_CONTACT, SET_PROFILE_EMAIL, SET_PROFILE_LOGO_URL, SET_PROFILE_NAME, SET_USER_LIST,
} from '../actions/types';

const initialState = {

  // Dropdown expense

  isVisibleDropdown: '',
  isDropdownEnabled: false,

  DropdownEnabled: false,
  category_list: [
    { label: 'Clothing', value: 'Clothing' },
    { label: 'Drinks', value: 'Drinks' },
    { label: 'Education', value: 'Education' },
    { label: 'Food', value: 'Food' },
    { label: 'Fuel', value: 'Fuel' },
    { label: 'Fun', value: 'Fun' },
    { label: 'Hospital', value: 'Hospital' },
    { label: 'Hotel', value: 'Hotel' },
    { label: 'Medical', value: 'Medical' },
    { label: 'Merchandise', value: 'Merchandise' },
    { label: 'Movie', value: 'Movie' },
    { label: 'Other', value: 'Other' },
    { label: 'Personal', value: 'Personal' },
    { label: 'Pets', value: 'Pets' },
    { label: 'Restaurant', value: 'Restaurant' },
    { label: 'Shopping', value: 'Shopping' },
    { label: 'Tips', value: 'Tips' },
    { label: 'Transport', value: 'Transport' },
    { label: 'School Fee', value: 'School Fee' },
  ],

  category_type: 'Choose category ',

  payment_list: [
    { label: 'Bank', value: 'Bank' },
    { label: 'Cash', value: 'Cash' },
    { label: 'Salary', value: 'Salary' },
  ],
  payment_type: 'Choose payment type',

  is_visible: false,

  // custom alert
  is_delete: false,

  // set add income variable

  income_category_list: [
    { label: 'Business', value: 'Business' },
    { label: 'Loan', value: 'Loan' },
    { label: 'Salary', value: 'Salary' },
  ],

  income_amount: '',
  income_category: 'Choose category',
  income_desc: '',
  income_payment_type: '',

  // set add expense variable

  expense_amount: '',
  expense_category: '',
  expense_desc: '',
  expense_payment_type: '',

  // database inser expense var

  income_list: [],
  expense_list: [],
  users_list: [],

  // dashboard status var

  income: 0,
  expense: 0,
  balance: 0,

  // login var

  login_id: '',
  login_pass: '',
  is_visible_pass: false,

  // FORGOT PASSWORD
  is_forgotPassword: false,
  isEnabled: false,
  is_visible_conf_pass: false,

  // sign Up
  name: '',
  email: '',
  phone_no: '',
  pass: '',
  conf_pass: '',

  // deposit var

  deposit_from_list: [
    { label: 'Cash', value: 'Cash' },
    { label: 'Bank', value: 'Bank' },
  ],
  // deposit_from_type:'choose type',
  deposit_from: 'Cash',
  deposit_to: 'Bank',
  deposit_amount: '',
  deposit_date: '',
  isVisibleDepositDate: false,
  deposit_time: '13:08 am',
  deposit_desc: '',

  // database inser deposit var

  deposit_list: [],

  //  Profile screen
  profile:null,
  profile_name: '',
  profile_contact: 237893196,
  profile_email: '',
  profile_address: '',
  profile_logo_url: '',
  // Update profile
  // update_profile: '',
  // is_update_visible: false,

};

export default function (state = initialState, action) {
  switch (action.type) {

    case SET_DESC:
      return {
        ...state,
        desc: action.payload,
      };

    
    // DROPDOWN cases

    case SET_DROPDOWN_VISIBILITY:
      return {
        ...state,
        isVisibleDropdown: action.payload,
      };
    case IS_DROPDOWN_ENABLED:
      return {
        ...state,
        isDropdownEnabled: action.payload,
      };
    case SET_CATEGORY_TYPE:
      return {
        ...state,
        category_type: action.payload,
      };
    case SET_PAYMENT_TYPE:
      return {
        ...state,
        payment_type: action.payload,
      };
    case IS_DROPDOWN_ENABLED_2:
      return {
        ...state,
        DropdownEnabled: action.payload,
      };
    case IS_VISIBLE:
      return {
        ...state,
        is_visible: action.payload,
      };
    case IS_DELETE_MESSAGE:
      return {
        ...state,
        is_delete: action.payload,
      };
    // ADD INCOME CASES
    case SET_ADD_INCOME_AMOUNT:
      return {
        ...state,
        income_amount: action.payload,
      };
    case SET_ADD_INCOME_CATEGORY:
      return {
        ...state,
        income_category: action.payload,
      };
   
    case SET_ADD_INCOME_DESC:
      return {
        ...state,
        income_desc: action.payload,
      };
    case SET_ADD_INCOME_PAYMENT_TYPE:
      return {
        ...state,
        income_payment_type: action.payload,
      };

    // ADD EXPENSE CASES


    case SET_ADD_EXPENSE_AMOUNT:
      return {
        ...state,
        expense_amount: action.payload,
      };
    case SET_ADD_EXPENSE_CATEGORY:
      return {
        ...state,
        expense_category: action.payload,
      };
  
    case SET_ADD_EXPENSE_DESC:
      return {
        ...state,
        expense_desc: action.payload,
      };
    case SET_ADD_EXPENSE_PAYMENT_TYPE:
      return {
        ...state,
        expense_payment_type: action.payload,
      };
    // DATABASE INSERT INCOME CASES

    // case INSERT_INCOME:
    //   return {
    //     ...state,
    //     inser_income: action.payload
    //   }

    // DATABASE INSERT EXPENSE CASES

    // case INSERT_EXPENSE:
    //   return {
    //     ...state,
    //     insert_expense: action.payload
    //   }


    case SET_INCOME_LIST:
      return {
        ...state,
        income_list: action.payload
      }

    case SET_EXPENSE_LIST:
      return {
        ...state,
        expense_list: action.payload
      }

    // dashboard status cases

    case SET_INCOME:
      return {
        ...state,
        income: action.payload
      }
    case SET_EXPENSE:
      return {
        ...state,
        expense: action.payload
      }
    case SET_BALANCE:
      return {
        ...state,
        balance: action.payload
      }

      // sign up cases:
      case SET_USER_LIST:
        return {
          ...state,
          users_list: action.payload
        }



    // Login cases
    case SET_LOGIN_ID:
      return {
        ...state,
        login_id: action.payload,
      };
    case SET_LOGIN_PASS:
      return {
        ...state,
        login_pass: action.payload,
      };
    // Forgot password
    case IS_FORGOT_PASSWORD:
      return {
        ...state,
        is_forgotPassword: action.payload,
      };
    case IS_VISIBLE_PASS:
      return {
        ...state,
        is_visible_pass: action.payload,
      };
    case SET_KEYBOARD_AVOIDING:
      return {
        ...state,
        isEnabled: action.payload,
      };

    case IS_VISIBLE_CONF_PASS:
      return {
        ...state,
        is_visible_conf_pass: action.payload,
      };

    // sign up cases

    case SET_NAME:
      return {
        ...state,
        name: action.payload,
      };
    case SET_EMAIL:
      return {
        ...state,
        email: action.payload,
      };
    case SET_PHONE_NO:
      return {
        ...state,
        phone_no: action.payload,
      };
    case SET_PASSWORD:
      return {
        ...state,
        pass: action.payload,
      };
    case SET_CONF_PASS:
      return {
        ...state,
        conf_pass: action.payload,
      };

    // deposit cases:
    case SET_DEPOSIT_FROM:
      return {
        ...state,
        deposit_from: action.payload,
      };
    case SET_DEPOSIT_TO:
      return {
        ...state,
        deposit_to: action.payload,
      };
    case SET_DEPOSIT_AMOUNT:
      return {
        ...state,
        deposit_amount: action.payload,
      };
    case SET_DEPOSIT_DATE:
      return {
        ...state,
        deposit_date: action.payload,
      };
    case SET_DEPOSIT_DATE_VISIBLE:
      return {
        ...state,
        isVisibleDepositDate: action.payload,
      };
    case SET_DEPOSIT_TIME:
      return {
        ...state,
        deposit_time: action.payload,
      };
    case SET_DEPOSIT_DESC:
      return {
        ...state,
        deposit_desc: action.payload,
      };

    // DATABASE INSERT DEPOSIT CASES

    // case INSERT_DEPOSIT:
    //   return {
    //     ...state,
    //     insert_deposit: action.payload
    //   }

    case SET_DEPOSIT_LIST:
      return {
        ...state,
        deposit_list: action.payload
      }
// profile 

  // profile screen
  case SET_PROFILE_NAME:
    return {
      ...state,
      profile_name: action.payload,
    };

  case SET_PROFILE_CONTACT:
    return {
      ...state,
      profile_contact: action.payload,
    };

  case SET_PROFILE_EMAIL:
    return {
      ...state,
      profile_email: action.payload,
    };

  
  case SET_PROFILE_ADDRESS:
    return {
      ...state,
      profile_address: action.payload,
    };
    case SET_PROFILE_LOGO_URL:
      return {
        ...state,
        profile_logo_url: action.payload,
      };
      case SET_DASHBOARD_PROFILE:
        return {
          ...state,
          profile: action.payload,
        };
  // Update profile
  // case SET_UPDATE_PROFILE_PIC:
  //   return {
  //     ...state,
  //     update_profile: action.payload,
  //   };

  // case IS_UPDATE_PROFILE_VISIBLE:
  //   return {
  //     ...state,
  //     is_update_visible: action.payload,
  //   };

    default:
      return state;
  }
}
