import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, SafeAreaView, FlatList } from 'react-native';
import Header from '../component/Header';
import Colors from '../common/Colors';
import CustomDateTimePicker from '../component/CustomDateTimePicker';
import Font from '../common/Font';
import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { deleteAllIncome, handlePickerDate, hidePickerDate, isDeleteMessage, showPickerDate } from '../redux_store/actions/indexActions';
import DepositList_Row from '../screen_row/DepositList_Row';
import { handleDepositDatePicker, handleDepositTimePicker, hideDepositTimePicker, hidePickerDepositDate, showDepositTimePicker, showPickerDepositDate } from '../redux_store/actions/indexPickerActions';
import { SwipeListView } from 'react-native-swipe-list-view';
import { TouchableHighlight } from 'react-native-gesture-handler';
import { getWidth } from '../common/Layout';
class DepositScreen extends Component {
    constructor(props) {
        super(props)
        // this.props.deleteAllIncome()

    }

    _renderItemDeposit = (item) => {
        return (
            <DepositList_Row
                data_row={item.item}
            />
        )
    }
    _renderHiddenItem = (data, rowMap) => {
        return (
            <View
                style={[styles.mainViewStyle,]}
            >
                <View style={styles.swipeViewStyle} >
                    <TouchableHighlight
                        onPress={() => {

                        }
                        }
                        style={styles.swipeStyle}
                    >
                        <Text style={{ color: 'white' }} >{"Edit"}</Text>
                    </TouchableHighlight>

                    <TouchableHighlight
                        onPress={() => {
                            this._onDeleteDeposit()
                            this.props.isDeleteMessage(!this.props.is_delete)
                        }}
                        style={[styles.swipeStyle, { borderBottomRightRadius: 10, borderTopRightRadius: 10, backgroundColor: "red" }]}
                    >
                        <Text style={{ color: "white" }} >{"Delete"}</Text>
                    </TouchableHighlight>

                </View>

            </View>
        )
    }
    _onDeleteDeposit = () => {
        this.props.isDeleteMessage(true);
    }

    render() {
        console.log("income >>>>>>>");
        console.log(this.props.deposit_list);

        return (
            <SafeAreaView style={styles.container}>

                <Header
                    status="Withdraw/Deposit Report"
                    iconWidth={30}
                    iconHeight={30}
                    color={Colors.whiteText}
                    onPress={() => { this.props.navigation.goBack() }
                    }
                />

                <View style={styles.rowStyle}>
                    <Text style={styles.txtStyle}>
                        {'Date'}
                    </Text>
                    <CustomDateTimePicker
                        title={'Pick a date'}
                        value={this.props.deposit_date}
                        // value={'Date'}
                        onPress={this.props.showPickerDepositDate}
                        isVisible={this.props.isVisibleDepositDate}

                        onConfirm={(time) => {
                            this.props.handleDepositDatePicker(moment(time).format('YYYY-MM-DD'))
                            // this.props.hideDepositTimePicker
                        }}
                        right_icon={require('../assets/calendar_g.png')}
                        onCancel={this.props.hidePickerDepositDate}
                        mode={'date'}
                    />

                </View>
                {/* {this.props.deposit_amount == 0 ? <View style={{ flex: 1, height: 40, justifyContent: 'center', alignItems: 'center' }}>
                    <Image
                        style={{ height: 120, width: 120 }}
                        source={require('../assets/no_transaction.png')}
                    />

                    <Text>{'No Transactions'} </Text>
                    <Text>{'Please add your trasaction'} </Text>

                </View>

                    : */}

                <SwipeListView
                    data={this.props.deposit_list}
                    //  onEdit={this._onEdit}
                    renderItem={this._renderItemDeposit}
                    keyExtractor={(item, index) => 'key' + index}
                    renderHiddenItem={this._renderHiddenItem.bind(this)}
                    rightOpenValue={-180}
                    disableRightSwipe={true}
                    onRowOpen={(rowKey, rowMap) => {
                        setTimeout(() => {
                            rowMap[rowKey] && rowMap[rowKey].closeRow()
                        }, 1000)
                    }}
                />
                {/* <FlatList
                    data={this.props.deposit_list}
                    renderItem={this._renderItemDeposit}
                keyExtractor={(item, index) => item.income_id.toString()}
                /> */}
                {/* } */}



                <View
                    style={{
                        position: 'absolute',
                        zIndex: 1,
                        elevation: 5,
                        alignSelf: 'flex-end',
                        justifyContent: 'flex-end',
                        bottom: 0,
                        marginBottom: 10,
                    }}
                >
                    <TouchableOpacity
                        onPress={() =>
                            this.props.navigation.navigate('AddDepositScreen')
                        }
                        style={[styles.addbtnStyle, styles.myshadow]}>
                        <Image
                            style={{ height: 52, width: 52, marginRight: 20 }}
                            source={require('../assets/add1.png')}
                        />
                    </TouchableOpacity>
                </View>

            </SafeAreaView>
        );
    }
}



const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    rowStyle:
    {
        marginHorizontal: 5,
        marginTop: 2,
        justifyContent: 'space-between',
        backgroundColor: 'white',
        // alignItems: 'center'
    },
    imgStyle: {
        height: 30,
        width: 30,
        marginRight: 8
    },
    txtStyle: {
        fontSize: 14,
        color: 'black',
        fontFamily: Font.Roboto,
        fontWeight: 'bold',
        marginVertical: 5,
        marginHorizontal: 10
    },


    addbtnStyle: {
        width: 52,
        height: 52,
        marginRight: 25,
        alignSelf: 'flex-end',
    },
    myshadow: {
        shadowColor: '#0008',
        shadowOffset: {
            height: 1,
            width: 0,
        },
        shadowOpacity: 0.5,
        elevation: 11,
    },
    swipeViewStyle: {
        flexDirection: "row",
        width: getWidth(100),
        height: getWidth(82),
        alignItems: "center",
        justifyContent: 'space-around'
    },
    swipeStyle: {
        backgroundColor: "gray",
        flex: 1,
        // height:getWidth(30),
        width: getWidth(50),
        alignItems: "center",
        justifyContent: 'center'
    },
    mainViewStyle: {
        justifyContent: 'center',
        flex: 1,
        alignItems: "flex-end",
        marginHorizontal: 10,
        marginVertical: 6,
        backgroundColor: 'white',
        borderRadius: 10,
        marginRight: 12
    },

});

const mapStateToProps = (state) => {
    let s = state.indexReducer;
    const getState = state.indexPickerReducer;

    return {
        deposit_list: s.deposit_list,
        is_delete: s.is_delete,

        deposit_date: getState.deposit_date,
        isVisibleDepositDate: getState.isVisibleDepositDate,
        deposit_time: getState.deposit_time,
        isDepositTimeVisibility: getState.isDepositTimeVisibility,

    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            deleteAllIncome: () => deleteAllIncome(),
            isDeleteMessage: (is_delete) => isDeleteMessage(is_delete),

            // picker action
            hidePickerDepositDate: () => hidePickerDepositDate(),
            showPickerDepositDate: (date) => showPickerDepositDate(date),
            handleDepositDatePicker: (date) => handleDepositDatePicker(date),

            handleDepositTimePicker: (time) => handleDepositTimePicker(time),
            showDepositTimePicker: (isTrue) => showDepositTimePicker(isTrue),
            hideDepositTimePicker: (isTrue) => hideDepositTimePicker(isTrue),
        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(DepositScreen);