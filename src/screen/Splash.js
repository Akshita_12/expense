import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, SafeAreaView, PixelRatio } from 'react-native';

class Splash extends Component {
  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.navigate('DashboardScreen');
    }, 500);
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.container}>

          <TouchableOpacity>
            <Image
              style={{ height: PixelRatio.getPixelSizeForLayoutSize(80), width: PixelRatio.getPixelSizeForLayoutSize(80), }}
              source={require('../assets/expense.jpeg')}
            />
          </TouchableOpacity>
        </View>

      </SafeAreaView>
    );
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: '#4A148C'
  },
  txtStyle: {
    fontSize: 28,
    color: 'black',
    fontFamily: 'serif'
  },
  imgStyle: {
    height: 90,
    width: 90
  }
});


export default Splash;
