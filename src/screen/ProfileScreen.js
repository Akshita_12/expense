import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
  Image,
} from 'react-native';
import Font from '../common/Font';
import Color from '../common/Colors';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Header from '../component/Header';
import Colors from '../common/Colors';

import BottomNev from '../component/BottomNev';
import ProfileRow from '../component/ProfileRow';
import MyButton from '../common/MyButton';
import { getWidth } from '../common/Layout';
import ProfileTopComponent from '../component/ProfileTopComponent';
import { setProfileAddress, setProfileContact, setProfileEmail, setProfileLogo, setProfileName } from '../redux_store/actions/indexActions';


class ProfileScreen extends Component {
  constructor(props) {
    super(props);
    // debugLog(this.props.profile_logo_url)
  }

  _onLogout = () => {
    
    this.props.navigation.navigate('Login');

    //Reset profile reducer
    // this.props.setProfileName("")
    // this.props.setProfileContact("")
    // this.props.setProfileEmail("")
    // this.props.setProfileState("") 
    // this.props.setProfileStateId("")  //state_id
    // this.props.setProfileAddress("")
    // this.props.setProfileLogo("")
    // this.props.setProfileGoogleReview("")
  };

 
  _onEdit = () => {
    this.props.navigation.navigate("UpdateProfile")
    this.props.setImage("")
  }

  render() {
    return (
      <SafeAreaView style={[styles.container, { backgroundColor: Colors.backgroundColor }]}>
        <View
          style={[
            styles.container,
            { backgroundColor: Color.backgroundColor },
          ]}>
          <Header
            status="Profile"
            color={Colors.whiteText}
            onPress={() => this.props.navigation.goBack()}
            // image={require('../assets/menuvertical.png')}
            backVisible={true}
          />
          <View
            style={[{
              flex: 1,
              // backgroundColor: 'white',
              backgroundColor: '#fafaff',

              borderTopRightRadius: 30,
              borderTopLeftRadius: 30,
            }, styles.myshadow]}>
            <View style={{ flex: 1 }}>

              <ProfileTopComponent
                profile={{ uri: this.props.profile }}
                edit_icon={require('../assets/edit.png')}
                name={this.props.profile_name}
                id={this.props.vendor_id}
                logout_icon={require('../assets/logout.png')}
                onLogout={() => this._onLogout()}
                onClickEdit={() => {
                  this._onEdit()
                }}
              />

              <ProfileRow
                style={{ marginTop: getWidth(60) }}
                label="Email"
                value={this.props.profile_email}
              />
              <ProfileRow label="Phone number" value={this.props.profile_contact} />
              <ProfileRow label="Address" value={this.props.profile_address} />

           
            </View>

            <BottomNev
              onPressHome={() => {
                this.props.navigation.goBack();
              }}
              onPressUser={() => {
                this.props.navigation.navigate('ProfileScreen');
              }}
            //   onPressAdd={() => {
            //     this.props.navigation.navigate('AddScreen');
            //   }}
            //   image={require('../assets/add.png')}
              homeIcon={require('../assets/home1.png')}
              userIcon={{ uri: this.props.profile }}
              backgroundColor='white'
              color='white'
              right_label="Profile"
              left_label=""
            />
          </View>
        </View>
      
      </SafeAreaView>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#E5E5E5',
    backgroundColor: '#fafaff',
  },
  txt: {
    marginBottom: 7,
  },
  myshadow: {
    shadowColor: '#0008',
    shadowOffset: {
      height: 1,
      width: 0,
    },
    shadowOpacity: 0.5,
    elevation: 11,
  },
});

const mapStateToProps = (state) => {
  let s = state.indexReducer;
  return {
   
    profile_name: s.profile_name,
    profile_contact: s.profile_contact,
    profile_email: s.profile_email,
    profile_address: s.profile_address,
    profile: s.profile,
    profile_logo_url: s.profile_logo_url,

  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      setProfileName: (name) => setProfileName(name),
      setProfileContact: (contact) => setProfileContact(contact),
      setProfileEmail: (email) => setProfileEmail(email),
      setProfileAddress: (address) => setProfileAddress(address),
      setProfileLogo: (logo_url) => setProfileLogo(logo_url),
   
    //   setImage: (image) => setImage(image),
    },
    dispatch,
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);