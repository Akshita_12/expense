import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, SafeAreaView, Dimensions, FlatList } from 'react-native';
import Header from '../component/Header';
import Colors from '../common/Colors';
import Dashboard_Row from '../component/Dashboard_Row';
import { getWidth } from '../common/Layout';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setIsVisible } from '../redux_store/actions/indexActions';
import Font from '../common/Font';
import TransactionIncomeList_Row from '../screen_row/TransactionIncomeList_Row';
import TransactionExpenseList_Row from '../screen_row/TransactionExpenseList_Row';
import PremiumScreen from './PremiumScreen';
class AllTransactionScreen extends Component {

    _renderItemIncome = (item) => {
        return (
            <TransactionIncomeList_Row
                data_row={item.item}
            />
        )
    }

    _renderItemExpense = (item) => {
        return (
            <TransactionExpenseList_Row
                data_row={item.item}
            />
        )
    }

    render() {
        // console.log(this.props.expense_list);
        console.log("income >>>>>>>");
        console.log(this.props.income_list);

        return (
            <SafeAreaView style={styles.container}>
                <Header
                    status="All Transaction"
                    iconWidth={30}
                    iconHeight={30}
                    color={Colors.whiteText}
                    onPress={() => this.props.navigation.goBack()
                    }
                />
                <View style={styles.rowStyle}>

                    <View style={{ alignItems: "center", flex: 1, justifyContent: "center", borderWidth: 1, borderColor: '#E0E0E0' }} >
                        <Text style={[styles.txtStyle1, { fontWeight: 'bold', fontSize: 25 }]} > {"💸"}</Text>
                        <Text style={[styles.txtStyle1, { fontWeight: 'bold' }]} > {"Income"}</Text>
                        <Text style={[styles.txtStyle1, { fontSize: 11 }]}>{this.props.income} </Text>
                    </View>
                    <View style={{ alignItems: "center", flex: 1, justifyContent: "center", borderWidth: 1, borderColor: '#E0E0E0' }} >
                        <Text style={[styles.txtStyle2, { fontWeight: 'bold', fontSize: 25 }]}> {"🖖"}</Text>
                        <Text style={[styles.txtStyle2, { fontWeight: 'bold' }]} > {"Expense"}</Text>
                        <Text style={[styles.txtStyle2, { fontSize: 11 }]}>{this.props.expense} </Text>
                    </View>
                    <View style={{ alignItems: "center", flex: 1, justifyContent: "center", borderWidth: 1, borderColor: '#E0E0E0' }} >
                        <Text style={[styles.txtStyle3, { fontWeight: 'bold', fontSize: 25 }]}> {"💰"}</Text>
                        <Text style={[styles.txtStyle3, { fontWeight: 'bold' }]}>{" Balance"}</Text>
                        <Text style={[styles.txtStyle3, { fontSize: 11 }]}>{this.props.balance} </Text>
                    </View>

                </View>

                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: 10, marginHorizontal: 10 }}>
                    <TouchableOpacity
                        onPress={() => this.props.setIsVisible(true)}
                        style={{ flex: 1, flexDirection: 'row', height: 40, borderWidth: 1, borderColor: '#E0E0E0', justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                            style={styles.imgStyle}
                            source={require('../assets/sort.png')}
                        />

                        <Text style={{}}>
                            {'Sort'}
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('FilterScreen')}
                        style={{ flex: 1, flexDirection: 'row', height: 40, borderWidth: 1, borderColor: '#E0E0E0', justifyContent: 'center', alignItems: 'center' }}>

                        <Image
                            style={{ height: 20, width: 20, marginRight: 8 }}
                            source={require('../assets/filter.png')}
                        />
                        <Text >
                            {'Filter'}
                        </Text>
                    </TouchableOpacity>
                </View>


                <PremiumScreen />

                {/* flatlist */}

                {this.props.income == '' ? <View style={{ flex: 1, height: 40, justifyContent: 'center', alignItems: 'center' }}>
                    <Image
                        style={{ height: 120, width: 120 }}
                        source={require('../assets/no_transaction.png')}
                    />

                    <Text>{'No Transactions'} </Text>
                    <Text>{'Please add your trasaction'} </Text>

                </View> :
                    <View>
                        <FlatList
                            data={this.props.income_list}
                            renderItem={this._renderItemIncome}
                            // keyExtractor={(item, index) => item.income_id.toString()}
                            keyExtractor={(item, index) => index + 'key'}

                        />

                        <FlatList
                            data={this.props.expense_list}
                            renderItem={this._renderItemExpense}
                            keyExtractor={(item, index) => index + 'key'}

                        // keyExtractor={(item, index) => item.expense_id.toString()}
                        />
                    </View>

                }

                {
                    this.props.is_visible ? (
                        <View style={{
                            position: 'absolute',
                            flex: 1,
                            backgroundColor: '#0009',
                            width,
                            height,
                            justifyContent: 'center',
                        }}
                            onPress={() => this.props.setIsVisible(false)}
                        >
                            <View style={{
                                width: getWidth(230),
                                backgroundColor: 'white',
                                marginHorizontal: 65,
                                height: 340,
                                // justifyContent:'center',
                                borderRadius: 12,

                            }}>
                                <Text style={{ fontSize: 14, marginVertical: 20, alignSelf: 'center', fontFamily: Font.Roboto, fontWeight: 'bold', }}>
                                    {'Sort By'}
                                </Text>
                                <View style={{ borderTopWidth: 1, borderColor: '#E0E0E0', padding: 10, flex: 1, backgroundColor: 'white' }}>


                                    <TouchableOpacity style={{ flex: 1 }}>
                                        <Text>
                                            {'All'}
                                        </Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ flex: 1 }}>
                                        <Text>
                                            {'Today'}
                                        </Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ flex: 1 }}>
                                        <Text>
                                            {'Yesterday'}
                                        </Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ flex: 1 }}>
                                        <Text>
                                            {'Last 7 days'}
                                        </Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ flex: 1 }}>
                                        <Text>
                                            {'This Month'}
                                        </Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ flex: 1 }}>
                                        <Text>
                                            {'Last Month'}
                                        </Text>
                                    </TouchableOpacity>


                                </View>
                                <View style={{ flexDirection: 'row', borderTopWidth: 1, borderColor: '#E0E0E0', alignItems: 'center', height: 45 }}>
                                    <TouchableOpacity
                                        onPress={() => this.props.setIsVisible(false)}
                                        style={{ flex: 1, borderRightWidth: 1, borderColor: '#E0E0E0', height: 45, justifyContent: 'center' }}>

                                        <Text style={{ color: "#03A9F4", fontSize: 16, textAlign: 'center', }}>
                                            {'Cancel'}
                                        </Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ flex: 1, height: 45, justifyContent: 'center' }}>
                                        <Text style={{ color: "#03A9F4", fontSize: 16, textAlign: 'center', }}>
                                            {'Sort'}
                                        </Text>
                                    </TouchableOpacity>
                                </View>

                            </View>

                        </View>
                    ) : null}
            </SafeAreaView>
        );
    }
}


const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    rowStyle:
    {
        marginHorizontal: 5,
        marginTop: 2,
        flexDirection: 'row',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        height: getWidth(90),
        elevation: 8
    },
    imgStyle: {
        height: 30,
        width: 30,
        marginRight: 8
    },
    touch: {
        position: 'absolute',
        // flex: 1,
        backgroundColor: '#0009',
        width,
        height,
        justifyContent: 'flex-end',
    },
    txtStyle1: {
        fontSize: 14,
        color: Colors.greenColor,
        fontFamily: Font.LAOUI
    },
    txtStyle2: {
        fontSize: 14,
        color: Colors.redColor,
        fontFamily: Font.Roboto
    },
    txtStyle3: {
        fontSize: 14,
        color: Colors.orangeColor,
        fontFamily: Font.Roboto
    }
});

const mapStateToProps = (state) => {
    let s = state.indexReducer;

    return {

        is_visible: s.is_visible,
        income: s.income,
        expense: s.expense,
        expense_list: s.expense_list,
        income_list: s.income_list,
        balance: s.balance
    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setIsVisible: (IsVisible) => setIsVisible(IsVisible)
        },

        dispatch,
    );
};

export default connect(mapStateToProps, mapDsipatchToProps)(AllTransactionScreen);