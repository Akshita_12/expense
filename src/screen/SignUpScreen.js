import React, { Component } from 'react';
import {
  View,
  SafeAreaView,
  TouchableOpacity,
  Text,
  StyleSheet,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Colors from '../common/Colors';
import { getWidth } from '../common/Layout';
import MyInputText from '../common/MyInputText';
import SignUpLoginOption from '../component/SignUpLoginOption';
import { setConfirmPass, setConfPassVisibility, setEmail, setInsertSignUp, setKeyboardAvoiding, setName, setPass, setPasswordVisibility, setPhone } from '../redux_store/actions/indexActions';
import HeaderLarge from '../component/HeaderLarge';
import MyButton from '../common/MyButton';



class SignUpScreen extends Component {

  _onClickLogin = () => {
    this.props.navigation.navigate('ProfileScreen');
  };


  render() {
    return (

      // <KeyboardAvoidingView
      //   behavior={Platform.OS == "ios" ? "padding" : "height"}
      //   enabled
      //   style={{ backgroundColor: Colors.backgroundColor, flex: 1 }}>

      <SafeAreaView style={[styles.container, { flex: 1, backgroundColor: '#2979FF' }]}>

        <View keyboardShouldPersistTaps="always" style={styles.container}>
          <HeaderLarge
            title="Sign Up"
            iconWidth={20}
            iconHeight={20}
            color={Colors.whiteText}
          />
          <View style={{ flexDirection: "row", alignItems: "flex-end", alignSelf: 'flex-end' }} >
            <MyButton
              // onPress={() => this._onClickVendorProfile()}
              title="Sign up"
              height={35}
              containerStyle={{ width: "30%" }}
              txtStyle={{ color: 'white', fontSize: 14 }}
              style={{
                elevation: 0,
                marginTop: -40,
              }}
            />
          </View  >
          <ScrollView
            // scrollEnabled={false}
            style={{
              backgroundColor: 'white',
              borderTopLeftRadius: 30,
              borderTopRightRadius: 30,

            }}>
            <View
              //  keyboardDismissMode="on-drag"
              keyboardShouldPersistTaps={true}
            >

              <MyInputText
                label="User Name"
                autoCompleteType="username"
                onChangeText={(name) => this.props.setName(name)}
                left_icon={require('../assets/profile.png')}
                style={{ marginTop: 30, }}
                maxLength={15}
              />

              <MyInputText
                label="Email"
                autoCapitalize="none"
                keyboardType="email-address"
                onChangeText={(text) => this.props.setEmail(text)}
                left_icon={require('../assets/message.png')}
                style={{ marginTop: 10 }}
              />

              <MyInputText
                label="Phone"
                keyboardType="numeric"
                onChangeText={(text) => this.props.setPhone(text)}
                left_icon={require('../assets/phone.png')}
                style={{ marginTop: 10 }}
                maxLength={10}
                onFocus={this.props.setKeyboardAvoiding(true)}
              />
              <MyInputText
                label="Password"
                maxLength={15}
                onChangeText={(text) => {
                  this.props.setPass(text);
                }}
                secureTextEntry={this.props.is_visible_pass ? null : true}
                left_icon={require('../assets/password.png')}
                right_icon={
                  this.props.is_visible_pass
                    ? require('../assets/eye_off.png')
                    : require('../assets/eye_on.png')
                }
                onFocus={() => this.props.setKeyboardAvoiding(true)}
                onVisiblePass={() => {
                  this.props.setPasswordVisibility(!this.props.is_visible_pass);
                }}
                style={{ marginTop: 10 }}
              />

              <MyInputText
                label="Confirm Password"
                maxLength={15}
                onChangeText={(text) => {
                  this.props.setConfirmPass(text);
                }}
                secureTextEntry={this.props.is_visible_conf_pass ? null : true}
                left_icon={require('../assets/password.png')}
                right_icon={
                  this.props.is_visible_conf_pass
                    ? require('../assets/eye_off.png')
                    : require('../assets/eye_on.png')
                }
                onFocus={() => this.props.setKeyboardAvoiding(true)}
                onVisiblePass={() => {
                  this.props.setConfPassVisibility(!this.props.is_visible_conf_pass);
                }}
                style={{ marginTop: 10 }}

              />
            </View>

            {/* <View style={{height:100}} > */}

            <MyButton
              onPress={() => this.props.setInsertSignUp(
                this.props.name,
                this.props.email,
                this.props.phone_no,
                this.props.pass,
                this.props.conf_pass)}

              title="Sign Up"
              height={getWidth(44)}
              width={getWidth(345)}
              alignSelf="center"
              backgroundColor={Colors.textColor}
              marginVertical={10}
              color="white"
              fontSize={14}
              style={{}}
            />
            {/* </View> */}

            <SignUpLoginOption
              label="Already have an Account? "
              clickableText='Login here'
              onPress={() => this._onClickLogin()}
              style={{ paddingVertical: 10, }}
            />

          </ScrollView>
        </View>
        {/* {this.props.is_loading ? (
          <View
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#000a',
              elevation: 10,
              position: 'absolute',
            }}>
            <MyLoading />
          </View>
        ) : null} */}
      </SafeAreaView>
      // </KeyboardAvoidingView>

    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.textColor,
  },
  mainStyle: {
    justifyContent: 'center',
    flex: 1,
  },

  bottomTxtStyle: { color: Colors.textColor, fontSize: 12 },
  bottomView: {
    paddingTop: 15,
    paddingBottom: 15,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
});

const mapStateToProps = (state) => {
  const s = state.indexReducer;
  return {
    // is_loading: s.is_loading,
    name: s.name,
    email: s.email,
    phone_no: s.phone_no,
    pass: s.pass,
    conf_pass: s.conf_pass,
    is_visible_pass: s.is_visible_pass,
    is_visible_conf_pass: s.is_visible_conf_pass,
    isEnabled: s.isEnabled,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      setName: (name) => setName(name),
      setEmail: (email) => setEmail(email),
      setPhone: (phone) => setPhone(phone),
      setPass: (pass) => setPass(pass),
      setConfirmPass: (conf_pass) => setConfirmPass(conf_pass),
      // setName: (name) => sete(name),
      setKeyboardAvoiding: (isEnabled) => setKeyboardAvoiding(isEnabled),
      setPasswordVisibility: (isVisiblePass) => setPasswordVisibility(isVisiblePass),
      setConfPassVisibility: (isVisibleConfPass) => setConfPassVisibility(isVisibleConfPass),
      setInsertSignUp: (name, email, phone, pass, conf_pass) => setInsertSignUp(name, email, phone, pass, conf_pass)
    },
    dispatch,
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(SignUpScreen);
