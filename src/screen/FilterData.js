import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, SafeAreaView, Dimensions, FlatList, TextInput } from 'react-native';
import Colors from '../common/Colors';
import { getWidth } from '../common/Layout';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setIsVisible } from '../redux_store/actions/indexActions';
import Font from '../common/Font';
import RNIap, {
    InAppPurchase,
    PurchaseError,
    SubscriptionPurchase,
    acknowledgePurchaseAndroid,
    consumePurchaseAndroid,
    finishTransaction,
    finishTransactionIOS,
    purchaseErrorListener,
    purchaseUpdatedListener,
} from 'react-native-iap';
import MyButton from '../common/MyButton';

const productId = Platform.select({
    ios: ['bpmonthly', 'bpyear'],
    // android: ['com.boxpro.fitness.monthly1', 'com.boxpro.fitness.yearlyvideo1']
    android: ['tshirt1', 'tshirt2', 'tshirt3']
});

let purchaseUpdateSubscription;
let purchaseErrorSubscription;

class FilterData extends Component {

    constructor(props) {
        super(props);
        this.state = {
            user_list: [
                {
                    name: 'Alex',
                    ph_no: '123456',
                },
                {
                    name: 'Tom',
                    ph_no: '47557',

                },
                {
                    name: 'Ross',
                    ph_no: '123456',

                },

                {
                    name: 'Mark',
                    ph_no: '123456',

                },
                {
                    name: 'Tony',
                    ph_no: '123456',

                },
                {
                    name: 'Steve',
                    ph_no: '123456',

                },
                {
                    name: 'Krishna',
                    ph_no: '123456',
                },
            ],
            temp: [],
            search_txt: ''
        }
    }



    componentDidMount() {
        this.setState({
            temp: this.state.user_list
        })
    }


    renderItem = (dataItem) => {
        return (
            <View style={{ padding: 15 }}>
                <TouchableOpacity 
                onPress={()=>{
                    this.setState({
                        search_txt:dataItem.item.name
                    })
                }}
                >
                <Text>
                    {dataItem.item.name}
                </Text>
                </TouchableOpacity>
                <Text>
                    {dataItem.item.ph_no}
                </Text>
            </View>
        )
    }

    setSearch = (search_txt) => {
        let search_user = search_txt.toLowerCase()
        let filtered_name = this.state.temp.filter((item) => {
            let name = item.name.toLowerCase();
            return name.includes(search_user)
        })
        if (search_txt == '') {
            this.setState({
                temp: this.state.user_list
            })
        } else {
            this.setState({
                temp: filtered_name
            })
        }

    }


    render() {
        // console.log(this.props.expense_list);
console.log(this.state.search_txt);
        return (
            <View style={{ flex: 1, backgroundColor: 'azure' }}>

                <TextInput
                    placeholder="Search here"
                    backgroundColor="lightgreen"
                    onChangeText={(txt) => {
                        this.setSearch(txt),
                        this.setState({ search_txt: txt })
                    }}
                    value={this.state.search_txt}
                />

                <FlatList
                    data={this.state.temp}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => 'key' + index}
                />



            </View>
        );
    }
}


const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    rowStyle:
    {
        marginHorizontal: 5,
        marginTop: 2,
        flexDirection: 'row',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        height: getWidth(90),
        elevation: 8
    },
    imgStyle: {
        height: 30,
        width: 30,
        marginRight: 8
    },
    touch: {
        position: 'absolute',
        // flex: 1,
        backgroundColor: '#0009',
        width,
        height,
        justifyContent: 'flex-end',
    },
    txtStyle1: {
        fontSize: 14,
        color: Colors.greenColor,
        fontFamily: Font.LAOUI
    },
    txtStyle2: {
        fontSize: 14,
        color: Colors.redColor,
        fontFamily: Font.Roboto
    },
    txtStyle3: {
        fontSize: 14,
        color: Colors.orangeColor,
        fontFamily: Font.Roboto
    }
});

const mapStateToProps = (state) => {
    let s = state.indexReducer;

    return {

        is_visible: s.is_visible,
        income: s.income,
        expense: s.expense,
        expense_list: s.expense_list,
        income_list: s.income_list,
        balance: s.balance
    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setIsVisible: (IsVisible) => setIsVisible(IsVisible)
        },

        dispatch,
    );
};

export default connect(mapStateToProps, mapDsipatchToProps)(FilterData);