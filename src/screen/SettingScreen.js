import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, SafeAreaView, Switch } from 'react-native';
import { ScrollView, TextInput } from 'react-native';
import Colors from '../common/Colors';
import MyButton from '../common/MyButton';
import MySetting from '../component/MySetting';
import { getWidth } from '../common/Layout';
import Header from '../component/Header';
import { SwitchComponent } from '../component/SwitchComponent';
import { deleteAllExpense, deleteAllIncome, isDeleteMessage, setBalance, setExpense, setIncome } from '../redux_store/actions/indexActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import CustomAlert from '../component/CustomAlert';


class SettingScreen extends Component {
    constructor(props) {
        super(props)

    }
    
    onDelete = () => {
        this.props.deleteAllIncome(),
            this.props.deleteAllExpense(),
            this.props.setIncome(),
            this.props.setExpense(),
            this.props.setBalance(),

            alert('Clear all records successfully!')
    }
    componentWillUnmount() {
        this.props.isDeleteMessage(false)
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.container}>

                    <Header
                        status="Settings"
                        iconWidth={40}
                        iconHeight={32}
                        color={Colors.whiteText}
                        onPress={() => this.props.navigation.goBack()
                        }
                    />

                    <ScrollView>

                        <SwitchComponent />

                        <View style={{ marginHorizontal: 10, paddingHorizontal: 10, borderColor: 'grey', marginTop: 8 }}>
                            <Text style={{ color: Colors.textColor, fontWeight: 'bold', }}>{'Currency Symbol'} </Text>

                            <TextInput
                                placeholder="Currency Symbol"
                                style={{
                                }}
                            />

                        </View>

                        <MySetting
                            title="Date Format"
                            description="(DD-MMMM-YYYY)--->(05-Nov-2020) "
                        />

                        <MySetting
                            title="Time Format"
                            description="(hh:mm A)--->(4:33 PM) "
                        />

                        <MySetting
                            title="Category"
                            description="Add Category"
                        />

                        <MySetting
                            title="Payment Mode"
                            description="Add Payment Mode"
                        />

                        <MySetting
                            onPress={() => this.props.isDeleteMessage(true)}
                            title="Clear All Records"
                            description="This will clear your all income expense entries "
                        />

                        <MySetting
                            title="Suggestion"
                            description="Send Suggestion"
                        />

                        <MySetting
                            title="Contact Us"
                            description="support@appspedia.net "
                        />

                        <MySetting
                            title="App Versions"
                            description="Version 1.0.2 "
                        />

                    </ScrollView>

                    <MyButton
                        onPress={() => {
                            // alert(this.props.insert_expense)
                            // this._onPressSave()
                        }}
                        backgroundColor={Colors.textColor}
                        title="SAVE"
                        height={getWidth(44)}
                        width={getWidth(345)}
                        alignSelf="center"
                        marginVertical={5}
                        color="white"
                        fontSize={16}
                        style={{ marginBottom: getWidth(10), marginBottom: 30 }}
                    />

                </View>

                {
                    this.props.is_delete ?
                        <CustomAlert
                            is_delete={this.props.is_delete}
                            onPressFirst={() => {
                                this.onDelete(),
                                    this.props.isDeleteMessage(!this.props.is_delete)
                            }}
                            onPressSecond={() => this.props.isDeleteMessage(!this.props.is_delete)}
                            label_first="Yes"
                            label_second="Cancel"
                            msg="Are you sure do you really want to clear all records?"
                        />
                        : null
                }


            </SafeAreaView>
        );
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    txtStyle: {
        fontSize: 20,
        color: 'blue',
        fontFamily: 'serif'
    },
    imgStyle: {
        height: 90,
        width: 90
    }
});

const mapStateToProps = (state) => {
    let s = state.indexReducer;

    return {
        is_delete: s.is_delete,

    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            deleteAllIncome: () => deleteAllIncome(),
            deleteAllExpense: () => deleteAllExpense(),
            isDeleteMessage: (is_delete) => isDeleteMessage(is_delete),
            setIncome: () => setIncome(),
            setExpense: () => setExpense(),
            setBalance: () => setBalance()


        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(SettingScreen);
