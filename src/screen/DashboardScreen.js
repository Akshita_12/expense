import React, { Component } from "react";
import { FlatList, Image, Platform, SafeAreaView, TouchableOpacity, StyleSheet, Text, View, ScrollView, Alert } from "react-native";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import MyHeader from "../component/MyHeader";
import Dashboard_Row from "../component/Dashboard_Row";
import Status from "../component/Status";
import Font from "../common/Font";
import { setBalance, setExpense, setIncome } from "../redux_store/actions/indexActions";
import Colors from "../common/Colors";
import RNIap, {
    InAppPurchase,
    PurchaseError,
    SubscriptionPurchase,
    acknowledgePurchaseAndroid,
    consumePurchaseAndroid,
    finishTransaction,
    finishTransactionIOS,
    purchaseErrorListener,
    purchaseUpdatedListener,
} from 'react-native-iap';
import MyButton from "../common/MyButton";
import PlanButtons from "../component/PlanButtons";

const productId = Platform.select({
    ios: ['bpmonthly', 'bpyear'],
    // android: ['com.boxpro.fitness.monthly1', 'com.boxpro.fitness.yearlyvideo1']
    android: ['monthly_subscription1', 'tshirt2', 'ignite_flare_subscription_monthly1','ignite_unlimited_subscription_monthly1']

});


let purchaseUpdateSubscription;
let purchaseErrorSubscription;

class DashboardScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            products: [],
            receipt: '',

        }

        this.props.setIncome(),
            this.props.setExpense(),
            this.props.setBalance()
    }


    componentWillUnmount() {
        if (purchaseUpdateSubscription) {
            purchaseUpdateSubscription.remove();
            purchaseUpdateSubscription = null;
        }
        if (purchaseErrorSubscription) {
            purchaseErrorSubscription.remove();
            purchaseErrorSubscription = null;
        }
        RNIap.endConnection();
    }


    componentDidMount() {
        this.props.setIncome(),
            this._inAppPurchage()
    }

    _inAppPurchage = async () => {
        try {
            await RNIap.initConnection();
            console.log('Connected and refresh');
            const products = await RNIap.getProducts(productId);
            this.setState({ products: products });
            // console.log('products', products);
            if (Platform.OS == 'android') {
                await RNIap.flushFailedPurchasesCachedAsPendingAndroid();
            } else {
                await RNIap.clearTransactionIOS();
            }
        } catch (err) {
            console.log('++++', err.code, err.message);
        }


        purchaseUpdateSubscription = purchaseUpdatedListener(async purchase => {
            console.log('purchase', purchase);
            const receipt = purchase.transactionReceipt
                ? purchase.transactionReceipt
                : purchase.originalJson;

            if (receipt) {
                try {
                    const ackResult = await finishTransaction(purchase);
                    console.log('ackResult', ackResult);
                    if (ackResult.code == 'OK') {
                        this._hitIfPurchaseSuccess(receipt);
                    }
                } catch (ackErr) {
                    console.log('ackErr', ackErr);
                }
                console.log('receipt', receipt);
                this.setState({ receipt: receipt },);

            }
        });

        purchaseErrorSubscription = purchaseErrorListener(error => {
            console.log('purchaseErrorListener', error);
            Alert.alert('purchase error', JSON.stringify(error.message));
        });
    };





    _onRequestSubscription = async (productId) => {
        await RNIap.requestPurchase(productId)
            .then(res => {
                console.log('success!', res);
            })
            .catch(error => {
                console.log(error);
            });
    };


    _hitIfPurchaseSuccess = (receipt) => {
        let param = {
            receipt: receipt
        }
        this.props.sendRecceiptRequest(param, navigation)
    }





    _renderItem = ({ item, index }) => {
        console.log(typeof item.originalJson);
        let originalJson = JSON.parse(item.originalJson)
        const { Planstatus } = this.state;
        console.log(originalJson.name);
        return (
            <View style={styles.MainView}>
                <View style={styles.Container}>
                    <View style={{ width: '80%' }}>
                        <Text style={styles.TextStyleData}>
                            {originalJson.name}
                            <Text style={{ color: '#F37EA1' }}>
                                {originalJson.price == 0 ? null : '$' + item.price} for {item.duration}
                                {originalJson.productId.includes('monthly1') ? '1 Month' : null}
                            </Text>
                        </Text>
                    </View>
                    {item.title.toLowerCase() == 'Basic Free Plan'.toLowerCase() ? null : (
                        <PlanButtons
                            ButtonColor={'#5CEFC9'}
                            // style={{flexWrap:''}}
                            Textdata={
                                this.props.loginData?.data?.subscriptionPlanId?._id == item.productId
                                    ? 'Cancel'
                                    : 'Buy'
                            }
                            style={{
                                backgroundColor:
                                    this.props.loginData?.data?.subscriptionPlanId._id == item.productId
                                        ? 'red'
                                        : '#5CEFC9',
                                margin: 5,
                                borderRadius: 5,
                            }}
                            disabled={
                                Planstatus?.toUpperCase() === 'active'
                                    ? true
                                    : false
                            }
                            onPress={() => {
                                // console.log('SUBSCRIPTION_PLAN_I', item) // console.log('login_data', this.props.loginData?.data);
                                this.props.loginData?.data?.subscriptionPlanId?._id == item.productId
                                    ? this.cancelAlert()
                                    : this.props.loginData?.data?.isSubscriptionActive
                                        ? alert('You have already active subscription plan')
                                        :this._onRequestSubscription(item.productId)
                            }}
                        />
                    )}
                </View>
                <View style={styles.Sub_List}>
                    {/* {item.description.map((item, index) => {
                    return  */}
                    <View style={styles.Sub_mainView}>
                        {/* <View style={styles.sub_des}></View> */}
                        <Text style={{ alignSelf: 'center' }}> {item.description}</Text>
                    </View>
                    {/* })} */}
                </View>
            </View>
        );
    };


    render() {
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView style={styles.container}>

                    <MyHeader
                    />

                    <View style={{ flex: 0.5, justifyContent: 'center', margin: 5, alignItems: 'center', borderRadius: 180, }}>
                        <View style={{ height: 175, width: 175, backgroundColor: Colors.textColor, borderRadius: 200, justifyContent: 'center', alignItems: 'center' }}>
                            <View style={{ height: 110, width: 110, backgroundColor: 'white', borderRadius: 100, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 25, color: 'grey', fontFamily: 'monospace' }}>
                                    {'2021 '}
                                </Text>
                                <Text style={{ fontSize: 14, color: 'grey', fontFamily: 'serif' }}>
                                    {'JUNE'}
                                </Text>
                            </View>
                        </View>
                        <Text style={styles.txtStyle}>{'Current Month Chart - June 2021'} </Text>
                    </View>

                    <View style={{ flex: 1, }}>
                        <Status
                            incomes={this.props.income}
                            expenses={this.props.expense}
                            balance={this.props.balance}
                        />


                        {
                            this.state.products.length == 0 ?

                                <TouchableOpacity>
                                    <Text>Purchase</Text>
                                </TouchableOpacity>
                                :
                                <FlatList
                                    data={this.state.products}
                                    renderItem={this._renderItem}
                                    keyExtractor={(item, index) => index + 'key'}

                                />
                        }


                        {/* <View style={{ flex: 1, flexDirection: "row" }} >
                            <Dashboard_Row
                                onPress={() => { this.props.navigation.navigate('AddIncomeScreen') }}
                                image={require('../assets/moneybag.png')}
                                text="Add Income"
                            />

                            <Dashboard_Row
                                onPress={() => { this.props.navigation.navigate('AddExpenseScreen') }}
                                image={require('../assets/add_expense.png')}
                                text="Add Expenses"
                            />

                            <Dashboard_Row
                                onPress={() => { this.props.navigation.navigate('AllTransactionScreen') }}
                                image={require('../assets/transaction.png')}
                                text="Add Transactions"
                            />
                        </View>

                        <View style={{ flex: 1, flexDirection: "row" }} >
                            <Dashboard_Row
                                onPress={() => { this.props.navigation.navigate('DepositScreen') }}
                                image={require('../assets/withdraw.jpg')}
                                text="Withdraw/Deposit"
                            />
                            <Dashboard_Row
                                onPress={() => { this.props.navigation.navigate('SettingScreen') }}
                                image={require('../assets/setting3.png')}
                                text="Settings"
                            />
                            <Dashboard_Row
                                image={require('../assets/app_backup.png')}
                                text="App Backup"
                            />
                        </View>
                        <View style={{ flex: 1, flexDirection: "row" }} >
                            <Dashboard_Row
                                image={require('../assets/rate.png')}
                                text="Rate Us"
                            />
                            <Dashboard_Row
                                onPress={() => { this.props.navigation.navigate('ProfileScreen') }}
                                image={require('../assets/customer.png')}
                                text="Profile"
                            />
                            <Dashboard_Row
                                image={require('../assets/suggestions.png')}
                                text="Send Suggestion"
                            />
                        </View> */}
                    </View>
                </ScrollView>
            </SafeAreaView>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    txtStyle: {
        fontSize: 12,
        color: '#757575',
        fontFamily: Font.Roboto
    },
    TextStyle: {
        color: 'white',
        fontSize: 16,
    },
    MainView: {
        width: '100%',
        backgroundColor: 'white',
        borderRadius: 3,
        marginBottom: 10,
    },
    Container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 2,
        alignItems: 'center',
    },
    TextStyleData: {
        fontSize: 16,
        padding: 5,
        fontWeight: '700',
    },
    PlanText: {
        fontSize: 16,
        padding: 5,
        paddingHorizontal: 10,
    },
    Sub_List: {
        flexWrap: 'wrap',
        padding: '3%',
    },
    Sub_mainView: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: '2%',
        maxWidth: '100%',
    },
    sub_des: {
        height: 10,
        width: 10,
        borderRadius: 50,
        backgroundColor: 'black',
    },
    MainContainer: {
        flex: 1,
        backgroundColor: 'transparent',
    },
    sub_Container: {
        width: '100%',
        backgroundColor: 'transparent',
        justifyContent: 'space-around',
        paddingTop: '6%',
        borderBottomWidth: 5,
        borderColor: '#428F9F',
        paddingVertical: '5%',
    },
    HeaderContainer: {
        width: '85%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignSelf: 'center',
        paddingVertical: 5,
    },
    TimePeriodContainer: {
        width: '85%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignSelf: 'center',
        paddingVertical: 5,
    },

});
const mapStateToProps = (state) => {
    let getState = state.indexReducer;

    return {
        income: getState.income,
        expense: getState.expense,
        balance: getState.balance
    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setIncome: () => setIncome(),
            setExpense: () => setExpense(),
            setBalance: () => setBalance()
        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(DashboardScreen);