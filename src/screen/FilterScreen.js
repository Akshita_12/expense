import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, SafeAreaView, CheckBox, Dimensions, BackHandler, PixelRatio } from 'react-native';
import Header from '../component/Header';
import Colors from '../common/Colors';
import { getWidth } from '../common/Layout';
import MyButton from '../common/MyButton';
import CircleCheckBox, { LABEL_POSITION } from 'react-native-circle-checkbox';
import { isDeleteMessage } from '../redux_store/actions/indexActions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import CustomAlert from '../component/CustomAlert';

class FilterScreen extends Component {

    componentWillUnmount() {
        BackHandler.addEventListener('hardwareBackPress', this.backPressed);
        // Orientation.lockToPortrait()
    }
    backPressed = () => {

        this.props.isDeleteMessage(true)
        // return false;
    };
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <Header
                    status="Filter"
                    iconWidth={40}
                    iconHeight={22}
                    color={Colors.whiteText}
                    onPress={() => this.props.navigation.goBack()
                    }
                />
                <View style={styles.container}>

                    <MyButton
                        onPress={() => {
                        }}
                        backgroundColor="#9E9E9E"
                        title="Expense Type"
                        height={getWidth(44)}
                        width={getWidth(362)}
                        alignSelf="center"
                        color="white"
                        fontSize={16}
                    />

                    <View style={styles.rowStyle}>

                        <TouchableOpacity style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'space-around'
                        }}>

                            <CircleCheckBox
                                checked={true}
                                onToggle={(checked) => console.log('My state is: ', checked)}
                                labelPosition={LABEL_POSITION.RIGHT}
                                label="All"
                            />

                            <CircleCheckBox
                                checked={true}
                                onToggle={(checked) => console.log('My state is: ', checked)}
                                labelPosition={LABEL_POSITION.RIGHT}
                                label="Income"
                            />

                            <CircleCheckBox
                                checked={true}
                                onToggle={(checked) =>
                                    console.log('My state is: ', checked)
                                }
                                labelPosition={LABEL_POSITION.RIGHT}
                                label="Expenses"
                            />
                        </TouchableOpacity>

                    </View>

                    <MyButton
                        onPress={() => {

                        }}
                        backgroundColor="#9E9E9E"
                        title="Months"
                        height={getWidth(41)}
                        width={getWidth(362)}
                        alignSelf="center"
                        color="white"
                        fontSize={16}
                        alignSelf="flex-start"
                        style={{ marginBottom: getWidth(20), marginBottom: 10 }}

                    />

                    <MyButton
                        onPress={() => {

                        }}
                        backgroundColor="#9E9E9E"
                        title="Years"
                        height={getWidth(41)}
                        width={getWidth(362)}
                        alignSelf="center"
                        color="white"
                        fontSize={16}
                        alignSelf="flex-start"
                        style={{ marginBottom: getWidth(20), marginBottom: 10 }}

                    />

                    <MyButton
                        onPress={() => {

                        }}
                        backgroundColor="#9E9E9E"
                        title="Categories"
                        height={getWidth(41)}
                        width={getWidth(362)}
                        alignSelf="flex-start"
                        color="white"
                        fontSize={16}
                        style={{ marginBottom: getWidth(20), marginBottom: 10 }}

                    />

                    <MyButton
                        onPress={() => {

                        }}
                        backgroundColor={Colors.backgroundColor}
                        title="Payment Modes"
                        height={getWidth(41)}
                        width={getWidth(362)}
                        alignSelf="flex-start"
                        color="white"
                        fontSize={16}
                    />

                    <View style={{
                        marginTop: PixelRatio.getPixelSizeForLayoutSize(140)
                    }}>
                        <MyButton
                            onPress={() => {

                            }}
                            backgroundColor={Colors.backgroundColor}
                            title="Apply"
                            height={getWidth(44)}
                            width={getWidth(362)}
                            alignSelf="center"
                            // marginVertical={310}
                            color="white"
                            fontSize={16}
                            // style={{ marginBottom: getWidth(10), marginBottom: 10 }}
                        />
                    </View>

                </View>

                {



                    this.props.is_delete ?
                        <CustomAlert
                            is_delete={this.props.is_delete}
                            onPressFirst={() => {
                                this.props.isDeleteMessage(!this.props.is_delete)
                            }}
                            onPressSecond={() => this.props.isDeleteMessage(!this.props.is_delete)}
                            label_first="Yes"
                            label_second="Cancel"
                            msg="Are you sure do you really want to go out ?"
                        />
                        : null
                }

                {/* LOADING */}
                {this.props.is_loading ? (
                    <View
                        style={{
                            width: '100%',
                            height: '100%',
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: '#000a',
                            elevation: 10,
                            position: 'absolute',
                        }}>
                        <MyLoading
                            title="Please wait for a while..."
                        />
                    </View>
                ) : null}
            </SafeAreaView>
        );
    }
}
const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    rowStyle: {
        justifyContent: 'space-around',
        backgroundColor: 'white',
        elevation: 5,
        height: 56
    },
    checkbox: {
        alignSelf: "center",
        // outerColor:'#64B5F6',
        // innerColor:'white'
    },

    imgStyle: {
        height: 30,
        width: 30,
        marginRight: 8
    }

});
const mapStateToProps = (state) => {
    let s = state.indexReducer;

    return {

        is_delete: s.is_delete,
        // is_loading: s.is_loading,

    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            isDeleteMessage: (is_delete) => isDeleteMessage(is_delete),
        },

        dispatch,
    );
};
export default connect(mapStateToProps, mapDsipatchToProps)(FilterScreen);
