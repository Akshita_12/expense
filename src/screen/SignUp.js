import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, SafeAreaView, Dimensions, KeyboardAvoidingView, Platform } from 'react-native';
import { ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Colors from '../common/Colors';
import { getWidth } from '../common/Layout';
import LoginSignupButton from '../common/LoginSignupButton';
import MyButton from '../common/MyButton';
import MyInputText from '../common/MyInputText';
import { setConfirmPass, setConfPassVisibility, setEmail, setKeyboardAvoiding, setName, setPass, setPasswordVisibility, setPhone } from '../redux_store/actions/indexActions';
import { Bottom } from '../row_components/Bottom';
import { Top } from '../row_components/Top';

class SignUp extends Component {

    render() {
        return (
            <KeyboardAvoidingView
                behavior={Platform.OS == "ios" ? "padding" : "height"}
                enabled
                style={{ backgroundColor: Colors.backgroundColor, flex: 1 }}>

                <SafeAreaView style={styles.container}>
                    <View style={styles.container}>

                        <Top
                            title='Login'
                        />

                        <View style={{ elevation: 10, height: getWidth(300), marginHorizontal: 10, backgroundColor: 'white' }}>
                            <ScrollView  showsVerticalScrollIndicator={false}>
                                <MyInputText
                                    label="User Name"
                                    autoCompleteType="username"
                                    onChangeText={(name) => this.props.setName(name)}
                                    left_icon={require('../assets/user.png')}
                                    style={{ marginTop: 5, }}
                                    maxLength={15}
                                />
                                <MyInputText
                                    label="Email"
                                    autoCapitalize="none"
                                    keyboardType="email-address"
                                    onChangeText={(text) => this.props.setEmail(text)}
                                    left_icon={require('../assets/message.png')}
                                    style={{ marginTop: 10 }}
                                />

                                <MyInputText
                                    label="Phone"
                                    keyboardType="numeric"
                                    onChangeText={(text) => this.props.setPhone(text)}
                                    left_icon={require('../assets/phone.png')}
                                    style={{ marginTop: 10 }}
                                    maxLength={10}
                                    onFocus={this.props.setKeyboardAvoiding(true)}
                                />
                                <MyInputText
                                    label="Password"
                                    maxLength={15}
                                    onChangeText={(text) => {
                                        this.props.setPass(text);
                                    }}
                                    secureTextEntry={this.props.is_visible_pass ? null : true}
                                    left_icon={require('../assets/password.png')}
                                    right_icon={
                                        this.props.is_visible_pass
                                            ? require('../assets/eye_off.png')
                                            : require('../assets/eye_on.png')
                                    }
                                    onFocus={() => this.props.setKeyboardAvoiding(true)}
                                    onVisiblePass={() => {
                                        this.props.setPasswordVisibility(!this.props.is_visible_pass);
                                    }}
                                    style={{ marginTop: 10 }}
                                />

                                <MyInputText
                                    label="Confirm Password"
                                    maxLength={15}
                                    onChangeText={(text) => {
                                        this.props.setConfirmPass(text);
                                    }}
                                    secureTextEntry={this.props.is_visible_conf_pass ? null : true}
                                    left_icon={require('../assets/password.png')}
                                    right_icon={
                                        this.props.is_visible_conf_pass
                                            ? require('../assets/eye_off.png')
                                            : require('../assets/eye_on.png')
                                    }
                                    onFocus={() => this.props.setKeyboardAvoiding(true)}
                                    onVisiblePass={() => {
                                        this.props.setConfPassVisibility(!this.props.is_visible_conf_pass);
                                    }}
                                    style={{ marginTop: 10 }}

                                />
                            </ScrollView>

                        </View>


                        <LoginSignupButton
                            title='Sign Up'
                            onPress={()=>{}}
                        />

                        <Bottom
                            title='Login'
                            onPress={() => this.props.navigation.navigate('Login')}
                        />

                    </View>

                </SafeAreaView>
            </KeyboardAvoidingView>
        );
    }
}


const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        flex: 1,
        // height,
        // width,
        justifyContent: 'space-between',
        // alignItems: 'center',
        backgroundColor: '#fafaff'
    },
    txtStyle: {
        fontSize: 28,
        color: 'black',
        fontFamily: 'serif'
    },
    imgStyle: {
        height: 90,
        width: 90
    }
});

const mapStateToProps = (state) => {
    const s = state.indexReducer;
    return {
        // is_loading: s.is_loading,
        name: s.name,
        email: s.email,
        phone_no: s.phone_no,
        pass: s.pass,
        conf_pass: s.conf_pass,
        is_visible_pass: s.is_visible_pass,
        is_visible_conf_pass: s.is_visible_conf_pass,
        isEnabled: s.isEnabled,
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setName: (name) => setName(name),
            setEmail: (email) => setEmail(email),
            setPhone: (phone) => setPhone(phone),
            setPass: (pass) => setPass(pass),
            setConfirmPass: (conf_pass) => setConfirmPass(conf_pass),
            setKeyboardAvoiding: (isEnabled) => setKeyboardAvoiding(isEnabled),
            setPasswordVisibility: (isVisiblePass) => setPasswordVisibility(isVisiblePass),
            setConfPassVisibility: (isVisibleConfPass) => setConfPassVisibility(isVisibleConfPass)
        },
        dispatch,
    );
};
export default connect(mapStateToProps, mapDispatchToProps)(SignUp);

