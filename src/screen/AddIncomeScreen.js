import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, SafeAreaView, ScrollView, Dimensions, FlatList, Alert } from 'react-native';
import Header from '../component/Header';
import Colors from '../common/Colors';
import { getWidth } from '../common/Layout';
import MyInput from '../common/MyInput';
import Font from '../common/Font';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  hidePickerDate,
  showPickerDate, setDropdown,
  setDropdownVisibility, setCategoryType,
  setPaymentType, setDropdownVisibility2,
  setAddIncomeAmount, setAddIncomePaymentType,
  setAddIncomeCategory,
  setAddIncomeTime, setAddIncomeDesc, setInsertAddIncome,
  deleteAllIncome, setIncome, setExpense, setBalance
} from '../redux_store/actions/indexActions';
import MyButton from '../common/MyButton';
import CustomDateTimePicker from '../component/CustomDateTimePicker';
import moment, { ISO_8601 } from 'moment';
import MyDropDownOption from '../component/MyDropDownOption';
import { handleIncomePickerTime, hideIncomePicker, setAddIncomeDate, showIncomePicker, showPickerIncomeDate, hideIncomePickerDate } from '../redux_store/actions/indexPickerActions';


class AddIncomeScreen extends Component {
  constructor(props) {
    super(props)
    // this.props.deleteAllIncome()
    this.props.setIncome(),
      this.props.setExpense(),
      this.props.setBalance()
  }

  componentWillUnmount() {
    this.props.setDropdownVisibility(false)
  }

  _rendertCategory = (dataItem) => {
    return (
      <TouchableOpacity
        style={{ height: 40, justifyContent: "flex-start", alignItems: "center", elevation: 3, margin: 5, borderRadius: 10, backgroundColor: "white" }}
        onPress={() => {
          // this.props.setCategoryType(dataItem.item.value)
          this.props.setDropdownVisibility(false),
            this.props.setAddIncomeCategory(dataItem.item.value)
        }}
      >
        <Text style={{ marginTop: 8 }}>{dataItem.item.value} </Text>
      </TouchableOpacity>
    )
  }

  _renderPayment = (dataItem) => {
    return (
      <TouchableOpacity
        style={{
          height: 40,
          justifyContent: "center",
          alignItems: "center",
          elevation: 3,
          margin: 5,
          borderRadius: 10,
          backgroundColor: "white"
        }}
        onPress={() => {
          this.props.setPaymentType(dataItem.item.value)
          this.props.setDropdownVisibility2(false)
          this.props.setAddIncomePaymentType(dataItem.item.value)

        }}
      >
        <Text style={{ textAlign: 'center' }}>{dataItem.item.value} </Text>
      </TouchableOpacity>
    )
  }
  updateRecord = () => {
    this.props.setIncome(),
      this.props.setExpense(),
      this.props.setBalance()
  }

  showAlert() {
    Alert.alert(
      'Income',
      'Rocord added',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK', onPress: () => {
            this.updateRecord()
          }
        },
      ]
    );
  }

  _onPressSave = () => {
    this.props.setInsertAddIncome(this.props.income_amount, this.props.income_category, this.props.income_date, this.props.income_time, this.props.income_desc, this.props.income_payment_type)
    this.props.setIncome()
    // this.props.setExpense(),
    this.showAlert()
    this.props.navigation.navigate('DashboardScreen')
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Header
          status="Add Income"
          iconWidth={39}
          iconHeight={34}
          color={Colors.whiteText}
          onPress={() => { this.props.navigation.goBack(), this.props.setDropdownVisibility(false) }}
        />
        <ScrollView
          scrollEnabled={true}
          style={{
            // backgroundColor: 'white',
            flex: 1,
            marginBottom: 50
          }}>

          <Text style={styles.txtStyle}>
            {'Amount'}
          </Text>
          <MyInput
            value={this.props.income_amount}
            placeholder="Amount..."
            keyboardType="numeric"
            style={{ height: 40 }}
            onChangeText={(text) => {
              this.props.setAddIncomeAmount(text);
            }}
          />

          <MyDropDownOption
            label="Category"
            category_type={this.props.income_category}
            onPress={() => {
              this.props.setDropdownVisibility(true)
            }}
          />

          <Text style={styles.txtStyle}>
            {'Date'}
          </Text>
          <CustomDateTimePicker
            title={'Pick a date'}
            value={this.props.income_date}
            onPress={this.props.showPickerIncomeDate}
            isVisible={this.props.isVisibleAddIncomeDate}
            onConfirm={(time) =>
              this.props.setAddIncomeDate(
                moment(time).format('YYYY-MM-DD')
              )
            }
            right_icon={require('../assets/calendar_g.png')}
            onCancel={this.props.hideIncomePickerDate}
            mode={'date'}
            is24Hour={true}
          />


          <Text style={styles.txtStyle}>
            {'Time'}
          </Text>

          <CustomDateTimePicker
            title={'Pick a time'}
            value={this.props.income_time}
            onPress={this.props.showIncomePicker}
            isVisible={this.props.isIncomeTimeVisibility}
            onConfirm={(time) => {
              this.props.handleIncomePickerTime(moment(time).format('h:mm a'))
              this.props.hideIncomePicker
            }
            }
            onCancel={this.props.hideIncomePicker}
            mode={'time'}
            is24Hour={false}
            right_icon={require('../assets/time.png')}
          />


          <Text style={styles.txtStyle}>
            {'Description'}
          </Text>
          <MyInput
            value={this.props.income_desc}
            placeholder="Type your message..."
            keyboardType="default"
            multiline={true}
            textAlignVertical="top"
            maxLength={150}
            onChangeText={(text) => {
              this.props.setAddIncomeDesc(text);
            }}
            style={{
              height: getWidth(122),

            }}
          />
          <MyDropDownOption
            label="Payment Type"
            category_type={this.props.payment_type}

            onPress={() => {
              this.props.setDropdownVisibility2(true)
            }}
          />
        </ScrollView>

        <View
          style={{
            position: 'absolute',
            zIndex: 1,
            elevation: 15,
            alignSelf: 'center',
            justifyContent: 'center',
            bottom: 0,
            marginBottom: 1,
          }}>
          <MyButton
            onPress={() => {
              this._onPressSave()
            }}
            backgroundColor={Colors.textColor}
            title="SAVE"
            height={getWidth(44)}
            width={getWidth(345)}
            alignSelf="center"
            color="white"
            fontSize={16}
            style={{ borderRadius: 5 }}
          />
        </View>

        {
          this.props.isDropdownEnabled ?
            <View style={{
              backgroundColor: Colors.textColor, margin: 10,
              marginTop: 200,
              borderRadius: 10, position: "absolute"
            }} >
              <View
                onPress={() => this.props.setDropdownVisibility(false)}
                style={{
                  width: getWidth(340),
                  borderRadius: 10,
                }} >
                <FlatList
                  data={this.props.income_category_list}
                  renderItem={this._rendertCategory}
                  keyExtractor={(item, index) => 'key' + index}
                />
              </View>
            </View>
            :
            null
        }

        {
          this.props.DropdownEnabled ?
            <TouchableOpacity
              onPress={() => this.props.setDropdownVisibility2(false)}
              style={{ backgroundColor: "white", justifyContent: 'flex-end', width, height, position: "absolute" }} >

              <View style={{
                backgroundColor: Colors.textColor, paddingTop: 10,
                borderRadius: 10, elevation: 15,
                marginBottom: 80
              }}>
                <FlatList
                  data={this.props.payment_list}
                  renderItem={this._renderPayment}
                  keyExtractor={(item, index) => 'key' + index}
                />
              </View>

            </TouchableOpacity>
            :
            null
        }

      </SafeAreaView>
    );
  }
}

const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // height,
    // width,
    backgroundColor: 'white'
  },
  txtStyle: {
    fontSize: 14,
    color: 'black',
    fontFamily: Font.Roboto,
    fontWeight: 'bold',
    marginVertical: 5,
    marginHorizontal: 10
  },
  imgStyle: {
    height: getWidth(90),
    width: getWidth(90),

  }
});

const mapStateToProps = (state) => {
  const s = state.indexReducer;
  const getState = state.indexPickerReducer;
  return {

    isVisibleDropdown: s.isVisibleDropdown,
    isDropdownEnabled: s.isDropdownEnabled,
    income_category_list: s.income_category_list,
    category_type: s.category_type,
    payment_list: s.payment_list,
    payment_type: s.payment_type,
    DropdownEnabled: s.DropdownEnabled,

    income_amount: s.income_amount,
    income_category: s.income_category,
    income_date: getState.income_date,
    isVisibleAddIncomeDate: getState.isVisibleAddIncomeDate,
    income_time: getState.income_time,
    isIncomeTimeVisibility: getState.isIncomeTimeVisibility,

    income_desc: state.income_desc,
    income_payment_type: s.income_payment_type,


  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      hidePickerDate: () => hidePickerDate(),
      setDropdown: (isVisibleDropdown) => setDropdown(isVisibleDropdown),
      setDropdownVisibility: (isVisibleDropdown) => setDropdownVisibility(isVisibleDropdown),
      setCategoryType: (category_type) => setCategoryType(category_type),
      setPaymentType: (payment_type) => setPaymentType(payment_type),
      setDropdownVisibility2: (isVisible) => setDropdownVisibility2(isVisible),

      setAddIncomeAmount: (income) => setAddIncomeAmount(income),
      setAddIncomeCategory: (category) => setAddIncomeCategory(category),
      setAddIncomeTime: (time) => setAddIncomeTime(time),
      setAddIncomeDesc: (desc) => setAddIncomeDesc(desc),
      setAddIncomePaymentType: (payment) => setAddIncomePaymentType(payment),
      // database add income
      setInsertAddIncome: (amount, category, date, time, description, payment_type) => setInsertAddIncome(amount, category, date, time, description, payment_type),
      deleteAllIncome: () => deleteAllIncome(),
      setIncome: () => setIncome(),
      setExpense: () => setExpense(),
      setBalance: () => setBalance(),

      hideIncomePickerDate: () => hideIncomePickerDate(),
      setAddIncomeDate: (date) => setAddIncomeDate(date),
      showPickerIncomeDate: (date) => showPickerIncomeDate(date),
      handleIncomePickerTime: (time) => handleIncomePickerTime(time),
      showIncomePicker: (isTrue) => showIncomePicker(isTrue),
      hideIncomePicker: (isTrue) => hideIncomePicker(isTrue),
    },
    dispatch,
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(AddIncomeScreen);
