import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, SafeAreaView, Dimensions, Alert, KeyboardAvoidingView } from 'react-native';
import Header from '../component/Header';
import Colors from '../common/Colors';
import MyDropDownOption from '../component/MyDropDownOption';
import {
    deleteAllDeposit,
    deleteAllIncome,
    isDeleteMessage,
    setDepositAmount,
    setDepositDate,
    setDepositDesc,
    setDepositFrom,
    setDepositTime,
    setDepositTo,
    setDropdownVisibility2,
    setInsertAddDeposit,
    setIsVisible,
} from '../redux_store/actions/indexActions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { FlatList, ScrollView } from 'react-native';
import MyInput from '../common/MyInput';
import CustomDateTimePicker from '../component/CustomDateTimePicker';
import { getWidth } from '../common/Layout';
import moment from 'moment';
import MyButton from '../common/MyButton';
import Font from '../common/Font';
import { hideDepositTimePicker, showPickerDepositDate, hidePickerDepositDate, handleDepositDatePicker, handleDepositTimePicker, showDepositTimePicker } from '../redux_store/actions/indexPickerActions';

class AddDepositScreen extends Component {
    constructor(props) {
        super(props)
        // this.props.deleteAllDeposit()

    }

    showAlert() {
        Alert.alert(
            'Deposit',
            'Rocord added',
            [
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                {
                    text: 'OK', onPress: () => {
                    }
                },
            ]
        );
    }

    _onPressSave = () => {
        this.props.setInsertAddDeposit(
            this.props.deposit_from,
            this.props.deposit_to,
            this.props.deposit_amount,
            this.props.deposit_date,
            this.props.deposit_time,
            this.props.deposit_desc)
        // this.props.setIncome()
        this.showAlert()
        this.props.navigation.goBack()
    }

    _renderPayment = (dataItem) => {
        return (
            <TouchableOpacity
                style={{
                    height: 40,
                    justifyContent: "center",
                    alignItems: "center",
                    elevation: 3,
                    margin: 5,
                    borderRadius: 10,
                    backgroundColor: "white"
                }}
                onPress={() => {
                    this.props.setDepositFrom(dataItem.item.value)
                    this.props.setDropdownVisibility2(false)
                }}
            >
                <Text style={{ textAlign: 'center' }}>{dataItem.item.value} </Text>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "padding" : "height"}
                style={styles.container}
            >
                <SafeAreaView style={styles.container}>
                    <Header
                        status="Add Withdraw/Deposit "
                        iconWidth={30}
                        iconHeight={30}
                        color={Colors.whiteText}
                        onPress={() => { this.props.navigation.goBack() }
                        }

                    />
                    <View style={{ flexDirection: 'row', alignSelf: 'flex-end', marginRight: 15, marginTop: 5 }}>
                        <Text >{'Balance:'} </Text>
                        <Text style={{ fontWeight: 'bold' }}>{this.props.balance} </Text>
                    </View>

                    <ScrollView
                        scrollEnabled={true}
                        automaticallyAdjustContentInsets
                        style={{
                            // backgroundColor: 'white',
                            flex: 1
                        }}
                    >
                        <MyDropDownOption
                            style={{
                                marginVertical: 6
                            }}
                            label="From"
                            category_type={this.props.deposit_from}

                            onPress={() => {
                                this.props.setDropdownVisibility2(true)
                            }}
                        />
                        <MyDropDownOption
                            label="To"
                            category_type={'Bank'}
                            onPress={() => {
                                this.props.setIsVisible(true)
                            }}
                        />

                        <Text style={styles.txtStyle}>
                            {'Amount'}
                        </Text>

                        <MyInput
                            placeholder="Amount"
                            placeholderTextColor='#9e9e9e'
                            keyboardType="numeric"
                            style={{ height: 40 }}
                            onChangeText={(text) => {
                                this.props.setDepositAmount(text);
                            }}
                        />

                        <Text style={styles.txtStyle}>
                            {'Date'}
                        </Text>
                        <CustomDateTimePicker
                            title={'Pick a date'}
                            value={this.props.deposit_date}
                            // value={'Date'}
                            onPress={this.props.showPickerDepositDate}
                            isVisible={this.props.isVisibleDepositDate}

                            onConfirm={(time) => {
                                this.props.handleDepositDatePicker(moment(time).format('YYYY-MM-DD'))
                                this.props.hideDepositTimePicker
                            }}
                            right_icon={require('../assets/calendar_g.png')}
                            onCancel={this.props.hidePickerDepositDate}
                            mode={'date'}
                        />


                        <Text style={styles.txtStyle}>
                            {'Time'}
                        </Text>
                        <CustomDateTimePicker
                            title={'Pick a time'}
                            value={this.props.deposit_time}
                            onPress={this.props.showDepositTimePicker}
                            isVisible={this.props.isDepositTimeVisibility}

                            onConfirm={(time) => {
                                this.props.handleDepositTimePicker(moment(time).format('h:mm a'))
                                this.props.hideDepositTimePicker
                            }}
                            onCancel={this.props.hideDepositTimePicker}
                            mode={'time'}
                            is24Hour={true}
                            right_icon={require('../assets/time.png')}
                        />

                        <Text style={styles.txtStyle}>
                            {'Description'}
                        </Text>
                        <MyInput
                            placeholder="Type your message..."
                            keyboardType="default"
                            multiline={true}
                            textAlignVertical="top"
                            maxLength={150}
                            onChangeText={(text) => {
                                this.props.setDepositDesc(text);
                            }}
                            style={{
                                height: getWidth(122),

                            }}
                        />

                    </ScrollView>

                    <MyButton
                        onPress={() => {
                            // alert(this.props.insert_expense)
                            this._onPressSave()
                        }}
                        backgroundColor={Colors.textColor}
                        title="Add"
                        height={getWidth(44)}
                        width={getWidth(345)}
                        alignSelf="center"
                        marginVertical={5}
                        color="white"
                        fontSize={19}
                        style={{ marginBottom: getWidth(10), marginBottom: 30 }}
                    />


                    {
                        this.props.DropdownEnabled ?
                            <View
                                onPress={() => this.props.setDropdownVisibility2(false)}
                                style={{ backgroundColor: "white", justifyContent: 'flex-end', width, height, position: "absolute" }} >

                                <View style={{
                                    height: 200, backgroundColor: 'white', paddingTop: 20,
                                    borderTopRightRadius: 35, borderTopLeftRadius: 30, elevation: 10
                                }}>
                                    <FlatList
                                        data={this.props.deposit_from_list}
                                        renderItem={this._renderPayment}
                                        keyExtractor={(item, index) => index + 'key'}

                                    />
                                </View>

                            </View>
                            :
                            null
                    }

                    {
                        this.props.is_visible ? (
                            <TouchableOpacity style={{

                                backgroundColor: "white",
                                justifyContent: 'flex-end',
                                width,
                                height,
                                position: "absolute"

                            }}
                                onPress={() => this.props.setIsVisible(false)}
                            >
                                <View style={{
                                    height: 90,
                                    backgroundColor: 'white',
                                    paddingTop: 20,
                                    borderTopRightRadius: 35,
                                    borderTopLeftRadius: 30,
                                    elevation: 10,

                                }}>
                                    <Text style={[styles.txtStyle, { alignSelf: 'flex-start' }]}>
                                        {'Bank'}
                                    </Text>
                                </View>

                            </TouchableOpacity>
                        ) : null}

                    {
                        this.props.is_delete ?
                            <CustomAlert
                                is_delete={this.props.is_delete}
                                onPressFirst={() => {
                                    this.onDelete(),
                                        this.props.isDeleteMessage(!this.props.is_delete)
                                }}
                                onPressSecond={() => this.props.isDeleteMessage(!this.props.is_delete)}
                                label_first="Yes"
                                label_second="No"
                                msg="Are you sure do you want to delete selected items ?"
                            />
                            : null
                    }
                </SafeAreaView>
            </KeyboardAvoidingView>
        );
    }
}


const { width, height } = Dimensions.get('window')

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: 'white',
        // width,
        // height
    },
    rowStyle:
    {
        marginHorizontal: 5,
        marginTop: 2,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: 'white',
        // alignItems: 'center'
    },
    imgStyle: {
        height: 30,
        width: 30,
        marginRight: 8
    },
    addbtnStyle: {
        width: 52,
        height: 52,
        marginRight: 25,
        alignSelf: 'flex-end',
    },
    myshadow: {
        shadowColor: '#0008',
        shadowOffset: {
            height: 1,
            width: 0,
        },
        shadowOpacity: 0.5,
        elevation: 11,
    },
    txtStyle: {
        fontSize: 14,
        color: 'black',
        fontFamily: Font.Roboto,
        fontWeight: 'bold',
        marginVertical: 5,
        marginHorizontal: 10
    },


});

const mapStateToProps = (state) => {
    const s = state.indexReducer;
    const getState = state.indexPickerReducer;

    return {
        balance: s.balance,

        isVisibleDropdown: s.isVisibleDropdown,
        isDropdownEnabled: s.isDropdownEnabled,
        category_list: s.category_list,
        category_type: s.category_type,
        // payment_list: s.payment_list,
        // payment_type: s.payment_type,
        DropdownEnabled: s.DropdownEnabled,
        is_visible: s.is_visible,

        deposit_from_list: s.deposit_from_list,
        deposit_from: s.deposit_from,
        deposit_to: s.deposit_to,
        deposit_amount: s.deposit_amount,
        deposit_desc: s.deposit_desc,

        deposit_date: getState.deposit_date,
        isVisibleDepositDate: getState.isVisibleDepositDate,
        deposit_time: getState.deposit_time,
        isDepositTimeVisibility: getState.isDepositTimeVisibility,
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {

            showPicker: () => showPicker(),
            hidePicker: () => hidePicker(),
            setDropdownVisibility2: (isVisible) => setDropdownVisibility2(isVisible),
            setIsVisible: (IsVisible) => setIsVisible(IsVisible),

            setDepositFrom: (deposit_from_type) => setDepositFrom(deposit_from_type),
            setDepositTo: (to) => setDepositTo(to),
            setDepositAmount: (amount) => setDepositAmount(amount),
            setDepositTime: (time) => setDepositTime(time),
            setDepositDesc: (desc) => setDepositDesc(desc),
            setInsertAddDeposit: (deposit_from, deposit_to, deposit_amount, deposit_date, deposit_time, deposit_desc) =>
                setInsertAddDeposit(deposit_from, deposit_to, deposit_amount, deposit_date, deposit_time, deposit_desc),
            // deleteAllIncome: () => deleteAllIncome(),

            // picker action
            hidePickerDepositDate: () => hidePickerDepositDate(),
            showPickerDepositDate: (date) => showPickerDepositDate(date),
            handleDepositDatePicker: (date) => handleDepositDatePicker(date),

            handleDepositTimePicker: (time) => handleDepositTimePicker(time),
            showDepositTimePicker: (isTrue) => showDepositTimePicker(isTrue),
            hideDepositTimePicker: (isTrue) => hideDepositTimePicker(isTrue),

            deleteAllDeposit: () => deleteAllDeposit(),
            isDeleteMessage: (is_delete) => isDeleteMessage(is_delete),






        },
        dispatch,
    );
};
export default connect(mapStateToProps, mapDispatchToProps)(AddDepositScreen);