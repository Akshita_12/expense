import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, SafeAreaView, Dimensions, FlatList } from 'react-native';
import Colors from '../common/Colors';
import { getWidth } from '../common/Layout';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setIsVisible } from '../redux_store/actions/indexActions';
import Font from '../common/Font';
import RNIap, {
    InAppPurchase,
    PurchaseError,
    SubscriptionPurchase,
    acknowledgePurchaseAndroid,
    consumePurchaseAndroid,
    finishTransaction,
    finishTransactionIOS,
    purchaseErrorListener,
    purchaseUpdatedListener,
} from 'react-native-iap';
import MyButton from '../common/MyButton';

const productId = Platform.select({
    ios: ['bpmonthly', 'bpyear'],
    // android: ['com.boxpro.fitness.monthly1', 'com.boxpro.fitness.yearlyvideo1']
    android: ['tshirt1', 'tshirt2', 'tshirt3']
});

let purchaseUpdateSubscription;
let purchaseErrorSubscription;

class AllTransactionScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            products: [],
            receipt: '',

        }

    }

    componentWillUnmount() {
        if (purchaseUpdateSubscription) {
            purchaseUpdateSubscription.remove();
            purchaseUpdateSubscription = null;
        }
        if (purchaseErrorSubscription) {
            purchaseErrorSubscription.remove();
            purchaseErrorSubscription = null;
        }
        RNIap.endConnection();
    }

    componentDidMount() {
        this._inAppPurchage()
    }


    _inAppPurchage = async () => {
        try {
            await RNIap.initConnection();
            await RNIap.flushFailedPurchasesCachedAsPendingAndroid();
            console.log('Connected and refresh')
        } catch (err) {
            console.log(err.code, err.message);
        }

        try {
            const products = await RNIap.getSubscriptions(productId);
            // const products = await RNIap.getSubscriptions(itemSkus);
            console.log('Products', products);
            this.setState({ products: products })
        } catch (err) {
            console.log(err.code, err.message);
        }



        purchaseUpdateSubscription = purchaseUpdatedListener(
            async (purchase) => {
                console.log('purchase', purchase);
                const receipt = purchase.transactionReceipt
                    ? purchase.transactionReceipt
                    : purchase.originalJson;
                console.log(receipt);
                if (receipt) {
                    try {
                        const ackResult = await finishTransaction(purchase);
                        console.log('ackResult', ackResult);
                        this._hitIfPurchaseSuccess()

                    } catch (ackErr) {
                        // console.log('ackErr', ackErr);
                    }

                    // this.setState({ receipt: receipt }, () => this.goNext());
                }
            },
        );

        purchaseErrorSubscription = purchaseErrorListener(
            (error) => {
                // console.log('purchaseErrorListener', error);
                // Alert.alert('purchase error', JSON.stringify(error.message));
            },
        );
    }

    _onRequestSubscription = async (productId) => {
        await RNIap.requestPurchase(productId).then((res) => {
            //  console.log('success!');
        }).catch((error) => {
            // console.log(error)
        })
    }

    // goNext = () => {
    //     alert('success')

    // }
    renderItem = (item) => {
        return (
            <View>
                <Text>
                    {item.item.productId}
                </Text>
                <Text>
                    {item.item.currency}
                    <Text>
                        {item.item.originalPrice}
                    </Text>
                </Text>
                <Text>
                    {item.item.title}
                </Text>
                <Text>
                    {item.item.subscriptionPeriodAndroid}
                </Text>

                <MyButton
                    title="Purchase Now"
                    onPress={() => this._onRequestSubscription(item.item.productId)}
                    height={30}
                    backgroundColor={Colors.backgroundColor}
                    containerStyle={{ width: '40%', padding: 10, alignSelf: 'flex-end' }}
                    txtStyle={{ color: 'white', fontWeight: '700' }}
                />
            </View>
        )
    }
    render() {
        // console.log(this.props.expense_list);
        console.log("PREMIUM >>>>>>>");

        return (
            <View>

                {
                    this.state.products.length == 0 ?

                        <TouchableOpacity>
                            <Text>Purchase</Text>
                        </TouchableOpacity>
                        :
                        <FlatList
                            data={this.state.products}
                            renderItem={this.renderItem}
                            keyExtractor={(item,index)=>'key'+index}
                        />
                }


            </View>
        );
    }
}


const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    rowStyle:
    {
        marginHorizontal: 5,
        marginTop: 2,
        flexDirection: 'row',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        height: getWidth(90),
        elevation: 8
    },
    imgStyle: {
        height: 30,
        width: 30,
        marginRight: 8
    },
    touch: {
        position: 'absolute',
        // flex: 1,
        backgroundColor: '#0009',
        width,
        height,
        justifyContent: 'flex-end',
    },
    txtStyle1: {
        fontSize: 14,
        color: Colors.greenColor,
        fontFamily: Font.LAOUI
    },
    txtStyle2: {
        fontSize: 14,
        color: Colors.redColor,
        fontFamily: Font.Roboto
    },
    txtStyle3: {
        fontSize: 14,
        color: Colors.orangeColor,
        fontFamily: Font.Roboto
    }
});

const mapStateToProps = (state) => {
    let s = state.indexReducer;

    return {

        is_visible: s.is_visible,
        income: s.income,
        expense: s.expense,
        expense_list: s.expense_list,
        income_list: s.income_list,
        balance: s.balance
    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setIsVisible: (IsVisible) => setIsVisible(IsVisible)
        },

        dispatch,
    );
};

export default connect(mapStateToProps, mapDsipatchToProps)(AllTransactionScreen);