import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, SafeAreaView, ScrollView, Dimensions, FlatList, Alert, AsyncStorage } from 'react-native';
import Header from '../component/Header';
import Colors from '../common/Colors';
import { getWidth } from '../common/Layout';
import MyInput from '../common/MyInput';
import Font from '../common/Font';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  hidePicker, showPicker, setDropdown, setDropdownVisibility, setCategoryType, setPaymentType,
  setDropdownVisibility2, setAddExpenseCategory, setAddExpenseAmount,
  setAddExpenseTime, setAddExpenseDesc, setAddExpensePaymentType,
  setInsertAddExpense, hidePickerDate, setExpense, setBalance, setIncome
} from '../redux_store/actions/indexActions';
import MyButton from '../common/MyButton';
import CustomDateTimePicker from '../component/CustomDateTimePicker';
import moment from 'moment';
import MyDropDownOption from '../component/MyDropDownOption';
import { handleExpensePickerTime, hideExpensePicker, hideExpensePickerDate, setAddExpenseDate, showExpensePicker, showPickerExpenseDate } from '../redux_store/actions/indexPickerActions';


class AddExpenseScreen extends Component {
  constructor(props) {
    super(props)
    // this.props.deleteAllIncome()
    this.props.setIncome(),
      this.props.setExpense(),
      this.props.setBalance()
  }
  _rendertCategory = (dataItem) => {
    return (
      <TouchableOpacity
        style={{ height: 40, justifyContent: "flex-start", alignItems: "center", elevation: 3, margin: 5, borderRadius: 10, backgroundColor: "white" }}
        onPress={() => {
          this.props.setCategoryType(dataItem.item.value)
          this.props.setDropdownVisibility(false)
          this.props.setAddExpenseCategory(dataItem.item.value)
        }}
      >
        <Text style={{ textAlign: 'center', marginTop: 8 }}>{dataItem.item.value} </Text>
      </TouchableOpacity>
    )
  }

  _renderPayment = (dataItem) => {
    return (
      <TouchableOpacity
        style={{
          height: 40,
          justifyContent: "center",
          alignItems: "center",
          elevation: 3,
          margin: 5,
          borderRadius: 10,
          backgroundColor: "white"
        }}
        onPress={() => {
          this.props.setPaymentType(dataItem.item.value)
          this.props.setDropdownVisibility2(false)
          this.props.setAddExpensePaymentType(dataItem.item.value)

        }}
      >
        <Text style={{ textAlign: 'center' }}>{dataItem.item.value} </Text>
      </TouchableOpacity>
    )
  }

  updateRecord = () => {
    this.props.setIncome(),
      this.props.setExpense(),
      this.props.setBalance()
  }

  showAlert() {
    Alert.alert(
      'Income',
      'Rocord added',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK', onPress: () => {
            this.updateRecord()
          }
        },
      ]
    );
  }

  _onPressSave = () => {
    this.props.setInsertAddExpense(
      this.props.expense_amount,
      this.props.expense_category,
      this.props.expense_date,
      this.props.expense_time,
      this.props.expense_desc,
      this.props.expense_payment_type)
    this.props.setExpense()
    // this.props.setIncome()
    this.showAlert()
    this.props.navigation.navigate('DashboardScreen')
  }

  _storeData = async () => {
    try {
      await AsyncStorage.setItem(
        '@balance',
        '725375128'
      );
      console.log('balance saved');
    } catch (error) {
      // Error saving data
      console.log(error);
    }
  };

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('@balance');
      if (value !== null) {
        // We have data!!
        console.log(value);
      }
    } catch (error) {
      // Error retrieving data
    }
  };
  

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Header
          status="Add Expenses"
          iconWidth={30}
          iconHeight={30}
          color={Colors.whiteText}
          onPress={() => this.props.navigation.goBack()}
        />
        <ScrollView
          scrollEnabled={true}
          style={{
            // backgroundColor: 'white',
            marginBottom: 50,
            flex: 1
          }}>

          <Text style={styles.txtStyle}>
            {'Amount'}
          </Text>

          <MyInput
            value={this.props.expense_amount}
            placeholder="Amount"
            keyboardType="numeric"
            style={{ height: 40 }}
            onChangeText={(text) => {
              this.props.setAddExpenseAmount(text);
            }}
          />

          <MyDropDownOption
            label="Category"
            category_type={this.props.category_type}
            onPress={() => {
              this.props.setDropdownVisibility(true)
            }}
          />

          <Text style={styles.txtStyle}>
            {'Date'}
          </Text>
          <CustomDateTimePicker
            title={'Pick a date'}
            value={this.props.expense_date}
            onPress={this.props.showPickerExpenseDate}
            isVisible={this.props.isVisibleAddExpenseDate}
            onConfirm={(time) =>
              this.props.setAddExpenseDate(
                moment(time).format('YYYY-MM-DD')
              )
            }
            right_icon={require('../assets/calendar_g.png')}
            onCancel={this.props.hideExpensePickerDate}
            mode={'date'}
            is24Hour={true}
          />

          <Text style={styles.txtStyle}>
            {'Time'}
          </Text>

          <CustomDateTimePicker
            title={'Pick a time'}
            value={this.props.expense_time}
            onPress={this.props.showExpensePicker}
            isVisible={this.props.isExpenseTimeVisibility}

            onConfirm={(time) => {
              this.props.handleExpensePickerTime(moment(time).format('h:mm a'))
              this.props.hideExpensePicker
            }}
            onCancel={this.props.hideExpensePicker}
            mode={'time'}
            is24Hour={true}
            right_icon={require('../assets/time.png')}
          />


          <Text style={styles.txtStyle}>
            {'Description'}
          </Text>
          <MyInput
            value={this.props.expense_desc}
            placeholder="Type your message..."
            keyboardType="default"
            multiline={true}
            textAlignVertical="top"
            maxLength={150}
            onChangeText={(text) => {
              this.props.setAddExpenseDesc(text);
            }}
            style={{
              height: getWidth(122),

            }}
          />

          <MyDropDownOption
            label="Payment Type"
            category_type={this.props.payment_type}

            onPress={() => {
              this.props.setDropdownVisibility2(true)
            }}
          />
        </ScrollView>




        <View
          style={{
            position: 'absolute',
            zIndex: 1,
            elevation: 15,
            alignSelf: 'center',
            justifyContent: 'center',
            bottom: 0,
            marginBottom: 1,
          }}>
          <MyButton
            onPress={() => {
              // alert(this.props.expense_date)
              // this._storeData();
              this._retrieveData()
              // this._onPressSave()
            }}
            backgroundColor={Colors.textColor}
            title="SAVE"
            height={getWidth(44)}
            width={getWidth(345)}
            alignSelf="center"
            color="white"
            fontSize={16}
            style={{ borderRadius: 5 }}
          />

        </View>

        {
          this.props.isDropdownEnabled ?
            <View
              onPress={() => this.props.setDropdownVisibility(false)}
              style={{ backgroundColor: "white", width, height, position: "absolute" }} >
              <FlatList
                data={this.props.category_list}
                renderItem={this._rendertCategory}
                keyExtractor={(item, index) => 'key' + index}
              />
            </View>
            :
            null
        }

        {
          this.props.DropdownEnabled ?
            <View
              onPress={() => this.props.setDropdownVisibility2(false)}
              style={{ backgroundColor: "white", justifyContent: 'flex-end', width, height, position: "absolute" }} >

              <View style={{
                backgroundColor: Colors.textColor, paddingTop: 10,
                borderRadius: 10, elevation: 15,
                marginBottom: 80
              }}>
                <FlatList
                  data={this.props.payment_list}
                  renderItem={this._renderPayment}
                  keyExtractor={(item, index) => 'key' + index}
                />
              </View>

            </View>
            :
            null
        }

      </SafeAreaView>
    );
  }
}

const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
  container: {
    // width,
    // height,
    flex: 1,
    backgroundColor: 'white'
  },
  txtStyle: {
    fontSize: 14,
    color: 'black',
    fontFamily: Font.Roboto,
    fontWeight: 'bold',
    marginVertical: 5,
    marginHorizontal: 10
  },

});

const mapStateToProps = (state) => {
  const s = state.indexReducer;
  const getState = state.indexPickerReducer;

  return {
    isVisibleDropdown: s.isVisibleDropdown,
    isDropdownEnabled: s.isDropdownEnabled,
    category_list: s.category_list,
    category_type: s.category_type,
    payment_list: s.payment_list,
    payment_type: s.payment_type,
    DropdownEnabled: s.DropdownEnabled,

    expense_amount: s.expense_amount,
    expense_category: s.expense_category,
    expense_date: getState.expense_date,
    isVisibleAddExpenseDate: getState.isVisibleAddExpenseDate,
    expense_time: getState.expense_time,
    isExpenseTimeVisibility: getState.isExpenseTimeVisibility,

    expense_desc: s.expense_desc,
    expense_payment_type: s.expense_payment_type,


  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      showPicker: () => showPicker(),
      setDropdown: (isVisibleDropdown) => setDropdown(isVisibleDropdown),
      setDropdownVisibility: (isVisibleDropdown) => setDropdownVisibility(isVisibleDropdown),
      setCategoryType: (category_type) => setCategoryType(category_type),
      setPaymentType: (payment_type) => setPaymentType(payment_type),
      setDropdownVisibility2: (isVisible) => setDropdownVisibility2(isVisible),

      setAddExpenseAmount: (income) => setAddExpenseAmount(income),
      setAddExpenseCategory: (category) => setAddExpenseCategory(category),
      setAddExpenseTime: (time) => setAddExpenseTime(time),
      setAddExpenseDesc: (desc) => setAddExpenseDesc(desc),
      setAddExpensePaymentType: (payment) => setAddExpensePaymentType(payment),
      setInsertAddExpense: (amount, category, date, time, description, payment_type) => setInsertAddExpense(amount, category, date, time, description, payment_type),
      setExpense: () => setExpense(),
      setBalance: () => setBalance(),
      setIncome: () => setIncome(),

      // date picker action
      hideExpensePickerDate: () => hideExpensePickerDate(),
      showPickerExpenseDate: (date) => showPickerExpenseDate(date),
      setAddExpenseDate: (date) => setAddExpenseDate(date),
      handleExpensePickerTime: (time) => handleExpensePickerTime(time),
      showExpensePicker: (isTrue) => showExpensePicker(isTrue),
      hideExpensePicker: (isTrue) => hideExpensePicker(isTrue),

    },
    dispatch,
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(AddExpenseScreen);
