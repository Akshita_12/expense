import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, SafeAreaView, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getWidth } from '../common/Layout';
import MyButton from '../common/MyButton';
import MyInputText from '../common/MyInputText';
import ForgotPassword from '../component/ForgotPassword';
import SignUpLoginOption from '../component/SignUpLoginOption';
import Colors from '../common/Colors';
import { setIsForgotPassword, setLoginId, setLoginPass, setPasswordVisibility } from '../redux_store/actions/indexActions';

import LoginSignupButton from '../common/LoginSignupButton';
import { Top } from '../row_components/Top';
import { Bottom } from '../row_components/Bottom';

class Login extends Component {

    _onForgotPassword = () => {
        if (this.props.login_id == '') {
            alert('Please type your email!');
            return;
        }
    }
    _onClickSignUp = () => {
        this.props.navigation.navigate('SignUp');
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.container}>

                    <Top
                        title='Login'
                    />

                    <View style={{ elevation: 10, height: getWidth(200), marginHorizontal: 10, backgroundColor: 'white' }}>

                        <MyInputText
                            label="EMAIL ID"
                            keyboardType="email-address"
                            autoCapitalize="none"
                            onChangeText={(text) => {
                                this.props.setLoginId(text);
                            }}
                            left_icon={require('../assets/email_g.png')}
                            style={{}}
                        />

                        <MyInputText
                            label="Enter password"
                            maxLength={15}
                            style={{}}
                            onChangeText={(text) => {
                                this.props.setLoginPass(text);
                            }}
                            secureTextEntry={this.props.is_visible_pass ? null : true}
                            left_icon={require('../assets/lock_g.png')}
                            right_icon={this.props.is_visible_pass ? require('../assets/eye_off.png') : require('../assets/eye_on.png')}
                            onVisiblePass={() => { this.props.setPasswordVisibility(!this.props.is_visible_pass) }}
                        />

                        <TouchableOpacity
                            onPress={() => this.props.setIsForgotPassword(true)}
                            style={{ alignSelf: 'flex-end', marginRight: 29, padding: 10 }}>
                            <Text style={{ fontSize: 12, color: Colors.backgroundColor }}>
                                {'Forgot Password'}
                            </Text>
                        </TouchableOpacity>

                    </View>

                    <LoginSignupButton
                        title='Login'
                        onPress={() => { this.props.navigation.navigate('DashboardScreen') }}
                    />

                    <Bottom
                        title='Sign Up'
                        onPress={() => this._onClickSignUp()}
                    />
                </View>

                {
                    this.props.is_forgotPassword ?
                        <ForgotPassword
                            label_first="Cancel"
                            label_second="Save"
                            value={this.props.login_id}
                            onPressFirst={() => { this.props.setIsForgotPassword(false) }}
                            onPressSecond={() => {
                                this._onForgotPassword();
                                this.props.setIsForgotPassword(!this.props.is_forgotPassword)
                            }}
                            onChangeTextEmail={(new_email) => this.props.setLoginId(new_email)}
                        />
                        : null
                }
            </SafeAreaView>
        );
    }
}


const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        flex: 1,
        // height,
        // width,
        justifyContent: 'space-between',
        // alignItems: 'center',
        backgroundColor: '#fafaff'
    },
    txtStyle: {
        fontSize: 28,
        color: 'black',
        fontFamily: 'serif'
    },
    imgStyle: {
        height: 90,
        width: 90
    }
});

const mapStateToProps = (state) => {
    const s = state.indexReducer;
    return {
        login_id: s.login_id,
        login_pass: s.login_pass,
        is_visible_pass: s.is_visible_pass,
        is_forgotPassword: s.is_forgotPassword,

    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setLoginId: (login_id) => setLoginId(login_id),
            setLoginPass: (login_pass) => setLoginPass(login_pass),
            setPasswordVisibility: (isVisible) => setPasswordVisibility(isVisible),
            setIsForgotPassword: (isTrue) => setIsForgotPassword(isTrue),
        },
        dispatch,
    );
};
export default connect(mapStateToProps, mapDispatchToProps)(Login);

