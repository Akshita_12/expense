import { TouchableOpacity, Text, View, Image, StyleSheet } from 'react-native';
import React from 'react';
import Colors from './Colors';

const LoginSignupButton = (props) => {
  return (

    <TouchableOpacity
      onPress={props.onPress}
      style={[styles.touch, styles.touch]} >
      <Text style={styles.txtStyle}>
        {props.title}
      </Text>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({

  myshadow: {
    shadowColor: "#1050e6",
    shadowOpacity: 0.15,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 8.30,
    elevation: 13,
  },
  touch: {
    backgroundColor: Colors.backgroundColor,
    alignSelf: 'flex-end',
    justifyContent: "center",
    height: 45,
    width: '55%',
    borderRadius: 80,
    marginRight: -28,
    elevation: 22
  },
  txtStyle: {
    fontWeight: 'bold',
    fontSize: 16,
    textAlign: 'center',
    color: 'white',
  }
});
export default LoginSignupButton;