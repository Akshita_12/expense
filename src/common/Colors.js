const tintColor = '#66ffff';
const whiteText = '#ffffff';
const statusBarColor = '#1050E6';
// const backgroundColor = '#1050E6';
const backgroundColor = '#5a03fc'
const placeHolderColor = '#C4C4C4';
const textColor = '#5a03fc';
const textDarkColor = "#3D3D3D"
const progressColor = '#9E9E9E';
const grayColor = '#737373';
const greenColor = '#1B5E20';
const redColor = '#F44336';
const orangeColor = '#FFA726'


export default {
  tintColor,
  tabIconDefault: '#384659',
  tabIconSelected: tintColor,
  whiteText,
  progressColor,
  statusBarColor,
  red:"red",
  grayColor,
  backgroundColor,
  placeHolderColor,
  textColor,
  textDarkColor,
  greenColor,
  redColor,
  orangeColor,
  red:'#ED1C24',
orange:'#F9A72B'
};
