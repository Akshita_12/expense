const Roboto = 'Roboto-Black';
const LAOUI = 'LaoUI';
const LAOUI_BOLD = 'LaoUI-Bold';
const DancingScipt = 'DancingScript-SemiBold';
const fontSize = 14;
const SourceSansPro = 'SourceSansPro-SemiBold';
const SourceSansProLight = 'SourceSansPro-Light';


export default {
  Roboto,
  LAOUI,
  LAOUI_BOLD,
  DancingScipt,
  fontSize,
  inputFont: 17,
  SourceSansPro,
  SourceSansProLight,
};