import {TouchableOpacity, Text, StyleSheet, View, Image} from 'react-native';
import React from 'react';
import TextInput from 'react-native-material-textinput';
import Font from './Font';
import Colors from './Colors';

const MyInputText = (props) => {
  return (
  
      <View style={[styles.rowStyle, props.style]}>
        {props.left_icon != null ? (
          <Image
            source={props.left_icon}
            style={{
              height: 20,
              width: 20,
              resizeMode: 'contain',
              marginRight: 13,
            }}
          />
        ) : null}
        <View style={{flex: 1}}>
          <TextInput
            fontSize={14}
            height={71}
            label={props.label}
            color={Colors.textColor}
            labelActiveScale={0.5}
            autoCompleteType={props.autoCompleteType}
            placeholder={props.placeholder}
            keyboardType={props.keyboardType}
            secureTextEntry={props.secureTextEntry}
            multiline={props.multiline}
            textAlignVertical={props.textAlignVertical}
            placeholderTextColor={Colors.placeHolderColor}
            placeholderColor={Colors.placeHolderColor}
            activeColor={Colors.textColor}
            onChangeText={props.onChangeText}
            underlineHeight={0}
            underlineActiveHeight={0}
            value={props.value}
            editable={props.editable}
            autoCapitalize = {props.autoCapitalize}
            maxLength={props.maxLength}
            onFocus={props.onFocus}
          />
        
        </View>
        <TouchableOpacity onPress={props.onVisiblePass}>
          <Image
            source={props.right_icon}
            style={{
              width: 20,
              height: 20,
              resizeMode: 'contain',
              marginRight: 13,
            }}
          />
        </TouchableOpacity>
      </View>

  );
};

const styles = StyleSheet.create({
  mainStyle: {
    alignItems: 'center',
    height:71,
    paddingHorizontal:10
  },
  rowStyle: {
    flexDirection: 'row',
    borderBottomColor: 'lightgray',
    borderBottomWidth: 0.4,
    marginHorizontal: 39,
    justifyContent: 'center',
    alignItems: 'center',
    height:71,
 
  },
  txt: {
    flex: 1,
    fontSize: Font.fontSize,
  },
});

export default MyInputText;
