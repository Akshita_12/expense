import { TextInput, View } from 'react-native';
import React from 'react';

const MyInput = (props) => {
  return (
    <TextInput
      placeholder={props.placeholder}
      value={props.value}
      keyboardType={props.keyboardType}
      placeholderTextColor={props.placeholderTextColor}
      multiline={props.multiline}
      textAlignVertical={props.textAlignVertical}
      maxLength={props.maxLength}
      style={[
        {
          height:props.height,
          elevation: 3,
          borderRadius: 5,
          fontSize: 14,
          backgroundColor: 'white',
          // fontFamily: 'serif',
          // borderWidth: 0.5,
          // borderColor: 'gray',
          marginHorizontal:7,
          marginVertical:10,
          paddingHorizontal:6,
        
          
        },
        props.style,
      ]}
      onChangeText={props.onChangeText}
    />
  );
};

export default MyInput;
