import { TouchableOpacity, Text, View, Image, StyleSheet } from 'react-native';
import React from 'react';

const MyButton = (props) => {
  return (
    <View style={[props.containerStyle]}>
      <TouchableOpacity
        onPress={props.onPress}
        style={[styles.myshadow,
        {
          backgroundColor:props.backgroundColor,
          height: props.height,
          width: props.width,
          borderRadius: props.borderRadius,
          alignItems: 'center',
          marginTop: props.marginTop,
          marginRight:props.marginRight,
          marginBottom: props.marginBottom,
          marginVertical: props.marginVertical,
          marginHorizontal: props.marginHorizontal,
          paddingHorizontal: props.paddingHorizontal,
          alignSelf: props.alignSelf,
          justifyContent: 'center',
          borderRadius:5,
        },
        props.style,
        ]}>
        <Text
          style={[
            { fontSize: props.fontSize, color: props.color },
            props.txtStyle,
          ]}>
          {props.title}
        </Text>
        {props.iconVisible ? (
          <View style={styles.imageIconStyle} >
            <Image
              style={{ width: 15, height: 15, resizeMode: "contain" }}
              source={props.image}
            />
          </View>
        ) : null}
      </TouchableOpacity>
    </View>
  );
};
const styles = StyleSheet.create({

  myshadow:{
    shadowColor:"#1050e6",
    shadowOpacity:0.15,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 8.30,
    // elevation: 2,
  },

  imageIconStyle: {
    // height: 25,
    // marginLeft: 10,
    // width: 25,
    // borderRadius: 30,
    // backgroundColor: "#48BB36",
    // justifyContent: "center",
    // alignItems: 'center'
  },

});
export default MyButton;