import React, { Component } from "react";
import { StatusBar, View } from "react-native";
import { Platform } from "react-native";
import { Provider } from "react-redux";
import { createStackNavigator } from '@react-navigation/stack'
import { createDrawerNavigator } from '@react-navigation/drawer'
import { NavigationContainer } from '@react-navigation/native'
import store from "./src/redux_store/store";
import Splash from "./src/screen/Splash";
import DashboardScreen from "./src/screen/DashboardScreen";
import AddExpenseScreen from "./src/screen/AddExpenseScreen";
import AllTransactionScreen from "./src/screen/AllTransactionScreen";
import DepositScreen from "./src/screen/DepositScreen";
import FilterScreen from "./src/screen/FilterScreen";
import AddDepositScreen from "./src/screen/AddDepositScreen";
import { run_database_migrations } from "./src/database/AppDatabase";
import ProfileScreen from "./src/screen/ProfileScreen";
import AddIncomeScreen from "./src/screen/AddIncomeScreen";
import SignUpScreen from "./src/screen/SignUpScreen";
import Colors from "./src/common/Colors";
import SettingScreen from "./src/screen/SettingScreen";
import Login from "./src/screen/Login";
import SignUp from "./src/screen/SignUp";
import PremiumScreen from "./src/screen/PremiumScreen";
import FilterData from "./src/screen/FilterData";


const Stack = createStackNavigator()
const Drawer = createDrawerNavigator();

function HomeStack() {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="Splash" component={Splash} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="SignUp" component={SignUp} />
      <Stack.Screen name="SignUpScreen" component={SignUpScreen} />
      <Stack.Screen name="DashboardScreen" component={DashboardScreen} />

    </Stack.Navigator>
  );
}

/**
 * Render the Home Drawer with a custom drawerContent.
 */
function HomeDrawer() {
  return (
    <Drawer.Navigator
      initialRouteName="Home"
    // drawerContent={(props) => <CustomDrawerContent {...props} />}
    >
      <Drawer.Screen name='HomeStack' component={HomeStack} />
      <Stack.Screen name="AddIncomeScreen" component={AddIncomeScreen} />
      <Stack.Screen name="AddExpenseScreen" component={AddExpenseScreen} />
      <Stack.Screen name="AllTransactionScreen" component={AllTransactionScreen} />
      <Stack.Screen name="DepositScreen" component={DepositScreen} />
      <Stack.Screen name="FilterScreen" component={FilterScreen} />
      <Stack.Screen name="AddDepositScreen" component={AddDepositScreen} />
      <Stack.Screen name="ProfileScreen" component={ProfileScreen} />
      <Stack.Screen name="SettingScreen" component={SettingScreen} />
      <Stack.Screen name="PremiumScreen" component={PremiumScreen}/>
      <Stack.Screen name="FilterData" component={FilterData}/>


      

    </Drawer.Navigator>
  );
}

class App extends Component {
  constructor(props) {
    super(props);
    run_database_migrations().then(() => {
      // console.log("creating table");
    });
  }
  render() {
    return (
      <Provider store={store} >
        <View style={{ flex: 1 }}>

          <StatusBar backgroundColor={Colors.backgroundColor}
            barStyle={"light-content"}
          />

          <NavigationContainer>
            <HomeDrawer />
          </NavigationContainer>
        </View>
      </Provider>
    )
  }
}

export default App;

